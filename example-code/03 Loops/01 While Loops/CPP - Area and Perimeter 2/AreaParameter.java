import java.util.Scanner;

public class AreaParameter
{
    static Scanner input = new Scanner( System.in );
    
    public static void main( String args[] )
    {
        boolean isDone = false;

        while (isDone == false)
        {
            double width, length;

            System.out.print( "Please enter the width: " );
            width = input.nextDouble();

            System.out.print( "Please enter the length: " );
            length = input.nextDouble();

            double area = width * length;

            double perimeter = (2 * width) + (2 * length);

            System.out.println();
            System.out.print( "Area is: \t" + area + " sqft"  );
            System.out.print( "Perimeter is: \t" + perimeter + " ft" );

            System.out.println();
            System.out.print( "Do you want to quit? (y/n): " );
            String choice;
            choice = input.next();

            if (choice.equals( "y" ) )
            {
                isDone = true;
            }

            System.out.println( "\n\n" );
        }
    }
}
