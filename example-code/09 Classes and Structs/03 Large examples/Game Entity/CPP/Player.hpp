#ifndef _PLAYER_HPP
#define _PLAYER_HPP

#include <string>
using namespace std;

class Player
{
public:
    Player()
    {
        m_name = "default";
        m_hp = 100;
    }

    void Setup( const string& name, int hp )
    {
        m_name = name;
        m_hp = hp;
    }

    bool IsAlive()
    {
        return ( m_hp > 0 );
    }

    // Accessors / Getters
    string GetName()
    {
        return m_name;
    }

    int GetHP()
    {
        return m_hp;
    }

    void TakeDamage( int amount )
    {
        m_hp -= amount;
    }



private:
    string m_name;
    int m_hp;
};

#endif
