#ifndef _REST_HPP
#define _REST_HPP

#include <iostream>
#include <string>
using namespace std;

struct Food
{
	string name;
	float price;
};

struct Menu
{
	Menu();

	Food breakfastList[3];
	Food lunchList[3];
	Food dinnerList[3];

	void Display();
	Food* GetItem(char type, int index);
};

#endif