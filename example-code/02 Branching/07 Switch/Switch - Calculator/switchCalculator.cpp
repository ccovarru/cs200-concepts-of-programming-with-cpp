#include <iostream>     // cin and cout
using namespace std;    // removes std:: prefix (std::cin, std::cout)

int main()
{
    float a, b, result;
    char operation;

    cout << "CALCULATOR" << endl;

    cout << "Enter number 1: ";
    cin >> a;

    cout << "Enter number 2: ";
    cin >> b;

    cout << "Enter a math operation (+, -, *, /): ";
    cin >> operation;

    // USING IF STATEMENTS
//    if ( operation == '+' )
//    {
//        result = a + b;
//    }
//    else if ( operation == '-' )
//    {
//        result = a - b;
//    }
//    else if ( operation == '*' )
//    {
//        result = a * b;
//    }
//    else if ( operation == '/' )
//    {
//        if ( b == 0 )
//        {
//            cout << endl << "Cannot divide by zero!" << endl;
//            result = 0;
//        }
//        else
//        {
//            result = a / b;
//        }
//    }

    // USING SWITCH STATEMENT
    switch ( operation )
    {
        case '+':   // operation == '+'
            result = a + b;
        break;

        case '-':   // operation == '-'
            result = a - b;
        break;

        case '*':   // operation == '*'
            result = a * b;
        break;

        case '/':   // operation == '/'
            if ( b == 0 )
            {
                cout << endl << "Cannot divide by zero!" << endl;
                result = 0;
            }
            else
            {
                result = a / b;
            }
        break;
    }

    cout << endl << "Result: " << result << endl;

    return 0;
}
