#include <iostream>     // cin and cout
using namespace std;    // removes std:: prefix (std::cin, std::cout)

int main()
{
    int number;
    cout << "enter a number: ";
    cin >> number;

    if ( number < 0 )
    {
        // negative number
        number = -number;
    }

    cout << "The absolute value is: " << number << endl;

    return 0;
}
