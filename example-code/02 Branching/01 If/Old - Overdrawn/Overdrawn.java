import java.util.Scanner;

public class Overdrawn
{
    public static void main( String[] args )
    {
        Scanner input = new Scanner( System.in );
        
        System.out.print( "What is your account balance? " );
        double balance = input.nextDouble();
        
        if ( balance < 0 )
        {
            System.out.println( "You are overdrawn!" );
        }
    }
}
