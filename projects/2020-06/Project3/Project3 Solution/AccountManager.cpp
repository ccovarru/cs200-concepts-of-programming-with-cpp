#include "AccountManager.hpp"

#include <iostream>
using namespace std;

AccountManager::AccountManager()
{
    m_loggedInUser = -1;
    Load();
}

AccountManager::~AccountManager()
{
    Save();
}

bool AccountManager::UsernameExists( string username ) const
{
    for ( auto& account : m_accounts )
    {
        if ( account.GetUsername() == username )
        {
            return true;
        }
    }
    return false;
}

void AccountManager::AddAccount( string username, string plaintextPassword )
{
    size_t hashedPassword = HashPassword( plaintextPassword );
    Account newAccount;
    newAccount.Setup( username, hashedPassword );
    m_accounts.push_back( newAccount );
}

bool AccountManager::SignIn( string username, string plaintextPassword )
{
    if ( !ValidatePassword( username, plaintextPassword ) )
    {
        return false;
    }

    m_loggedInUser = GetIndexOfUser( username );
    return true;
}

string AccountManager::GetLoggedInUsername() const
{
    if ( m_loggedInUser == -1 )
    {
        return "";
    }

    return m_accounts[ m_loggedInUser ].GetUsername();
}

void AccountManager::Load()
{
    ifstream input( "accounts.dat" );
    if ( input.fail() )
    {
        return;
    }

    string username;
    size_t password;

    while ( input >> username >> password )
    {
        Account account( username, password );
        m_accounts.push_back( account );
    }

    cout << m_accounts.size() << " accounts loaded." << endl;
}

void AccountManager::Save()
{
    ofstream output( "accounts.dat" );
    for ( auto& account : m_accounts )
    {
        output << account.GetUsername() << " " << account.GetHashedPassword() << endl;
    }
    output.close();
}

size_t AccountManager::HashPassword( string plaintextPassword )
{
    hash<string> stringHash;
    return stringHash( plaintextPassword );
}

bool AccountManager::ValidatePassword( string username, string plaintextPassword )
{
    size_t hashedPassword = HashPassword( plaintextPassword );
    for ( auto& account : m_accounts )
    {
        if ( account.GetUsername() == username )
        {
            if ( account.ValidatePassword( hashedPassword ) )
            {
                return true;
            }
            else
            {
                break;
            }
        }
    }
    return false;
}

int AccountManager::GetIndexOfUser( string username )
{
    for ( unsigned int i = 0; i < m_accounts.size(); i++ )
    {
        if ( m_accounts[i].GetUsername() == username )
        {
            return i;
        }
    }
    return -1;
}
