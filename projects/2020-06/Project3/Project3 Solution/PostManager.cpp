#include "PostManager.hpp"

#include "Menu.hpp"

#include <iostream>
#include <fstream>
using namespace std;

PostManager::PostManager()
{
    Load();
}

PostManager::~PostManager()
{
    Save();
}

void PostManager::CreateNewPost( string username )
{
    cout << "Enter your post on the following lines." << endl;
    cout << "Enter POST by itself to finish and post the blep." << endl;
    Menu::DrawHorizontalBar( 80 );

    string line;
    string text = "";

    bool firstLine = true;
    while ( true )
    {
        getline( cin, line );
        if ( line == "POST" )
        {
            break;
        }
        if ( !firstLine )
        {
            text += "\n";
        }
        text += line;
        firstLine = false;
    }

    Post newPost( username, text );
    m_posts.push_back( newPost );

    cout << "Posted" << endl;
}

void PostManager::PrintAllPosts() const
{
    for ( auto& post : m_posts )
    {
        post.Display();
    }
}

void PostManager::Load()
{
    ifstream input( "posts.dat" );
    if ( input.fail() )
    {
        return;
    }

    string buffer;
    string author;
    string text;

    while ( input >> buffer )
    {
        if ( buffer == "AUTHOR" )
        {
            input >> author;
        }
        else if ( buffer == "TEXT_BEGIN" )
        {
            text = "";
            input.ignore();
            bool firstLine = true;
            while ( buffer != "TEXT_END" )
            {
                getline( input, buffer );

                if ( buffer != "TEXT_END" )
                {
                    if ( !firstLine )
                    {
                        text += "\n";
                    }

                    text += buffer;
                }
                firstLine = false;
            }

            Post newPost( author, text );
            m_posts.push_back( newPost );
        }
    }

    cout << m_posts.size() << " posts loaded." << endl;
}

void PostManager::Save()
{
    ofstream output( "posts.dat" );
    for ( auto& post : m_posts )
    {
        output << "AUTHOR " << post.GetAuthor() << endl;
        output << "TEXT_BEGIN" << endl << post.GetText() << endl << "TEXT_END" << endl << endl;
    }
    output.close();
}
