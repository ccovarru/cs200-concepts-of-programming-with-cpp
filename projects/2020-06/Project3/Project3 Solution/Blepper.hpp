#ifndef _BLEPPER_HPP
#define _BLEPPER_HPP

#include "AccountManager.hpp"
#include "PostManager.hpp"
#include "Menu.hpp"

enum ProgramState { SIGNED_OUT, CREATE_ACCOUNT, LOG_IN, DASHBOARD, NEW_POST, VIEW_POSTS, QUIT };

class BlepperApp
{
    public:
    BlepperApp();

    void Run();

    private:
    bool m_done;
    ProgramState m_state;
    AccountManager m_accountManager;
    PostManager m_postManager;

    ProgramState Menu_SignIn();
    ProgramState Menu_CreateAccount();
    ProgramState Menu_Login();
    ProgramState Menu_Dashboard();
    ProgramState Menu_NewPost();
    ProgramState Menu_ViewPosts();

    string InputUsername();
    string InputPassword();
};

#endif
