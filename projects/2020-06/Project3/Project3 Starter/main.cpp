#include <iostream>
using namespace std;

#include "Blepper.hpp"

int main()
{
    BlepperApp app;
    app.Run();

    return 0;
}
