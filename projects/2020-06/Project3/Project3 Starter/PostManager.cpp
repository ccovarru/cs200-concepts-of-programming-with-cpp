#include "PostManager.hpp"

#include "Menu.hpp"

#include <iostream>
#include <fstream>
using namespace std;

PostManager::PostManager()
{
}

PostManager::~PostManager()
{
}

void PostManager::PrintAllPosts() const
{
}

void PostManager::CreateNewPost( string username )
{
}

/* Already written functions **********************************************/

void PostManager::Load()
{
    ifstream input( "posts.dat" );
    if ( input.fail() )
    {
        return;
    }

    string buffer;
    string author;
    string text;

    while ( input >> buffer )
    {
        if ( buffer == "AUTHOR" )
        {
            input >> author;
        }
        else if ( buffer == "TEXT_BEGIN" )
        {
            text = "";
            input.ignore();
            bool firstLine = true;
            while ( buffer != "TEXT_END" )
            {
                getline( input, buffer );

                if ( buffer != "TEXT_END" )
                {
                    if ( !firstLine )
                    {
                        text += "\n";
                    }

                    text += buffer;
                }
                firstLine = false;
            }

            Post newPost( author, text );
            m_posts.push_back( newPost );
        }
    }

    cout << m_posts.size() << " posts loaded." << endl;
}

void PostManager::Save()
{
    ofstream output( "posts.dat" );
    for ( auto& post : m_posts )
    {
        output << "AUTHOR " << post.GetAuthor() << endl;
        output << "TEXT_BEGIN" << endl << post.GetText() << endl << "TEXT_END" << endl << endl;
    }
    output.close();
}
