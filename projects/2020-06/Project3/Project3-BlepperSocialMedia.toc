\contentsline {chapter}{\numberline {1}Project instructions}{2}
\contentsline {paragraph}{Topics:}{2}
\contentsline {chapter}{\numberline {2}Grading breakdown}{3}
\contentsline {chapter}{\numberline {3}Project 3: Blepper, social network}{4}
\contentsline {section}{\numberline {3.1}C++ Topics}{4}
\contentsline {subsection}{\numberline {3.1.1}size\_t}{4}
\contentsline {subsection}{\numberline {3.1.2}Enumerations}{4}
\contentsline {subsection}{\numberline {3.1.3}Vectors}{5}
\contentsline {paragraph}{Declaring a vector:}{5}
\contentsline {paragraph}{Adding to a vector:}{5}
\contentsline {paragraph}{Iterating through a vector:}{5}
\contentsline {paragraph}{Getting the amount of items in a vector:}{6}
\contentsline {section}{\numberline {3.2}About: The program}{6}
\contentsline {subsection}{\numberline {3.2.1}Functionality}{6}
\contentsline {paragraph}{Signed-out screen:}{6}
\contentsline {paragraph}{Create new account screen:}{6}
\contentsline {paragraph}{Log-in screen:}{7}
\contentsline {paragraph}{Dashboard screen:}{7}
\contentsline {paragraph}{Post new blep screen:}{8}
\contentsline {paragraph}{View all bleps screen:}{8}
\contentsline {paragraph}{Account data file:}{9}
\contentsline {paragraph}{Post data file:}{9}
\contentsline {subsection}{\numberline {3.2.2}Program architecture}{10}
\contentsline {paragraph}{BlepperApp:}{10}
\contentsline {paragraph}{Accounts:}{11}
\contentsline {paragraph}{Posts:}{12}
\contentsline {section}{\numberline {3.3}Implementation}{14}
\contentsline {subsection}{\numberline {3.3.1}ProgramState (finished)}{14}
\contentsline {paragraph}{ProgramStates:}{14}
\contentsline {subsection}{\numberline {3.3.2}BlepperApp (finished)}{14}
\contentsline {paragraph}{Private member variables:}{14}
\contentsline {subsection}{\numberline {3.3.3}Account}{15}
\contentsline {paragraph}{Private member variables:}{15}
\contentsline {paragraph}{Account::Account()}{15}
\contentsline {paragraph}{Account::Account( string username, size\_t hashedPassword )}{15}
\contentsline {paragraph}{void Account::Setup( string username, size\_t hashedPassword )}{15}
\contentsline {paragraph}{bool Account::ValidatePassword( size\_t hashedPasswordAttempt ) const}{15}
\contentsline {paragraph}{string Account::GetUsername() const}{15}
\contentsline {paragraph}{size\_t Account::GetHashedPassword() const}{16}
\contentsline {subsection}{\numberline {3.3.4}AccountManager}{16}
\contentsline {paragraph}{Private member variables:}{16}
\contentsline {paragraph}{AccountManager::AccountManager()}{16}
\contentsline {paragraph}{AccountManager::\textasciitilde AccountManager()}{17}
\contentsline {paragraph}{void AccountManager::AddAccount( string username, string plaintextPassword )}{17}
\contentsline {paragraph}{bool AccountManager::UsernameExists( string username ) const}{18}
\contentsline {paragraph}{bool AccountManager::ValidatePassword( string username, string plaintextPassword )}{19}
\contentsline {paragraph}{int AccountManager::GetIndexOfUser( string username )}{19}
\contentsline {paragraph}{bool AccountManager::SignIn( string username, string plaintextPassword )}{20}
\contentsline {paragraph}{string AccountManager::GetLoggedInUsername() const}{20}
\contentsline {paragraph}{void AccountManager::Load()}{20}
\contentsline {paragraph}{void AccountManager::Save()}{20}
\contentsline {paragraph}{size\_t AccountManager::HashPassword(string plaintextPassword)}{20}
\contentsline {subsection}{\numberline {3.3.5}Testing Accounts}{21}
\contentsline {subsection}{\numberline {3.3.6}Post}{22}
\contentsline {paragraph}{Private member variables:}{22}
\contentsline {paragraph}{Post::Post()}{22}
\contentsline {paragraph}{Post::Post( string authorName, string text )}{22}
\contentsline {paragraph}{void Post::Setup( string authorName, string text )}{22}
\contentsline {paragraph}{void Post::Display() const}{22}
\contentsline {paragraph}{string Post::GetText() const}{23}
\contentsline {paragraph}{string Post::GetAuthor() const}{23}
\contentsline {subsection}{\numberline {3.3.7}PostManager}{23}
\contentsline {paragraph}{Private member variables:}{23}
\contentsline {paragraph}{PostManager::PostManager()}{23}
\contentsline {paragraph}{PostManager::\textasciitilde PostManager()}{23}
\contentsline {paragraph}{void PostManager::PrintAllPosts() const}{23}
\contentsline {paragraph}{void PostManager::CreateNewPost( string username )}{24}
\contentsline {paragraph}{void PostManager::Load()}{25}
\contentsline {paragraph}{void PostManager::Save()}{25}
\contentsline {subsection}{\numberline {3.3.8}Testing Posts}{26}
