\contentsline {chapter}{\numberline {1}Turn-in instructions}{2}
\contentsline {chapter}{\numberline {2}Setting up your IDE}{4}
\contentsline {chapter}{\numberline {3}Using Visual Studio}{6}
\contentsline {section}{\numberline {3.1}Downloading Visual Studio}{6}
\contentsline {section}{\numberline {3.2}Installing Visual Studio}{7}
\contentsline {section}{\numberline {3.3}Creating a program}{8}
\contentsline {section}{\numberline {3.4}Writing a program}{11}
\contentsline {section}{\numberline {3.5}Building and running the program}{12}
\contentsline {section}{\numberline {3.6}Locating your source file}{14}
\contentsline {chapter}{\numberline {4}Using Code::Blocks}{16}
\contentsline {section}{\numberline {4.1}Downloading Code::Blocks}{16}
\contentsline {section}{\numberline {4.2}Installing Code::Blocks}{17}
\contentsline {section}{\numberline {4.3}Creating a program}{17}
\contentsline {paragraph}{Project type selection screen:}{19}
\contentsline {paragraph}{Project information screen:}{19}
\contentsline {paragraph}{Compiler information screen:}{20}
\contentsline {section}{\numberline {4.4}Writing a program}{23}
\contentsline {section}{\numberline {4.5}Building and running the program}{24}
\contentsline {section}{\numberline {4.6}Locating your source file}{26}
