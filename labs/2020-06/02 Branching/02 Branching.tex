\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Branching}
\newcommand{\laTitle}       {CS 200 Lab}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ / \laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

\section*{Lab instructions}

\paragraph{Turn in:}

\begin{itemize}
	\item	Once done, upload the .cpp file that you edited: \texttt{branchinglab.cpp}.
	\item	Don't zip the source files.
	\item	Don't zip the entire folder and upload that. I only want source files.
\end{itemize}

\newpage
\section*{Branching lab}

\subsection*{Project setup}

	Some \textbf{starter code} is provided for you for this lab.
	You can download the starter file off the Canvas assignment, or type in the following:

\begin{lstlisting}[style=code]
#include <iostream>   // Use cin and cout
#include <string>     // Use strings
using namespace std;

void Program1()
{
}

void Program2()
{
}

void Program3()
{
}

void Program4()
{
}

void Program5()
{
}

// Don't modify main
int main()
{
    while ( true )
    {
        cout << "Run which program? (1-5): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
\end{lstlisting}

	~\\
	If you are downloading the file, make sure to create a new project/solution
	in Visual Studio/Code::Blocks first, and then move the \texttt{branchinglab.cpp}
	file into that folder.

	\paragraph{Add existing file in Visual Studio:}

	\begin{enumerate}
		\item	Right-click on your project file in the \textbf{Solution Explorer}.
		\item	Select \textbf{Add} and choose \textbf{Existing item...}
		\item	Find the file and add it to the project.
	\end{enumerate}

	\paragraph{Add existing file in Code::Blocks:}

	\begin{enumerate}
		\item	Right-click on your project file in the \textbf{Projects} pane.
		\item	Click \textbf{Add files...}
		\item	Find your file and click \textbf{Open}.
		\item	Make sure Debug and Release are both checked and click \textbf{OK}.
		\item	Make sure to save your project (File $>$ Save Project). 
	\end{enumerate}

\newpage
\subsection*{About the starter code}

	This starter code uses \textbf{functions}, which we have not covered
	yet in this class, but they are handy for breaking up programs into
	smaller, more managable chunks. \textbf{You do not need to modify main()}
	for this lab. There are five mini-programs you will be implementing,
	and they will go under each \textbf{Program()} function, just like
	how you'd code in \texttt{main()} - except there's no \texttt{return 0;}.
	
	~\\
	When you run the program, it will ask you which program you want to run.
	You can test out each program this way.
	
\begin{lstlisting}[style=output]
Run which program? (1-5): 1


Enter your hometown: OverlandPark
That's a long name!
Enter your name: JCCC
Hello JCCC from OverlandPark!

------------------------------------
Run which program? (1-5): 
\end{lstlisting}

\newpage
\subsection*{Program 1: Hometown (if statements)}

	\begin{itemize}
		\item	This program will ask the user what their name and their hometown is. 
		\item	If their hometown has a long name, it will comment ``that's a long name!'',	otherwise it won't add any extra remark about the name.
		\item	Finally, it will say ``Hello [NAME] from [HOMETOWN]!''
	\end{itemize}
	
	\paragraph{Variables:} ~\\
	
	\begin{center}	
		\begin{tabular}{l l l}
			\textbf{Variable name} 	& \textbf{Data type}	& \textbf{Description} \\ \hline
			\texttt{name}			& \texttt{string}		& The user's name \\
			\texttt{hometown}		& \texttt{string}		& The user's hometown
		\end{tabular}
	\end{center}
	
	\paragraph{Step-by-step (this program only):} ~\\
	\begin{enumerate}
		\item	Declare two string variables, \texttt{name} and \texttt{hometown}.
		\item	Output the text, ``Enter your hometown: '' using a \texttt{cout} statement.
		\item	Get input from the user and store it in \texttt{hometown} with the \texttt{cin} statement.
		\item	If the string length of \texttt{hometown} is more than 6 characters, then...
			\begin{enumerate}	
				\item	Output the text, ``That's a long name!'' using a \texttt{cout} statement.
			\end{enumerate}
		\item	Output the text, ``Enter your name: '' using a \texttt{cout} statement.
		\item	Get input from the user and store it in \texttt{name} with the \texttt{cin} statement.
		\item	Output the text, ``Hello [NAME] from [HOMETOWN]!'', swapping out the [NAME] and [HOMETOWN] text
				with the variable values.
	\end{enumerate}
	
	\paragraph{String lengths:} ~\\
	The \textbf{string} data type in C++ is actually a special object that
	contains functions to help make using strings easier - it is a smarter
	data type than normal ints, floats, and chars.
	
	You can get the length of a string (the amount of characters stored in a string)
	using the \texttt{.size()} function like this:
	
\begin{lstlisting}[style=code]
string myString = "abc";
// Outputs 3
cout << myString.size();	

myString = "1 2 3 4";
// Outputs 7 (counts the spaces)
cout << myString.size();	
\end{lstlisting}
	
	\texttt{.size()} is a function that belongs to \texttt{string} types.
	All functions have opening and closing parentheses after the function name
	(``size''), so don't forget the () when \textit{calling} this function.
	~\\
	
	You can store the length of the hometown in a new variable:
\begin{lstlisting}[style=code]
int townsize = hometown.size();
if ( townsize > 6 ) // ...	
\end{lstlisting}

	Or check the length directly in the if statement without using a variable:
\begin{lstlisting}[style=code]
if ( hometown.size() > 6 ) // ...	
\end{lstlisting}
	
	\paragraph{String inputs:} ~\\
	For this program, you can have the strings be inputted this way:
	
\begin{lstlisting}[style=code]
cin >> hometown;
\end{lstlisting}

	However, doing it this way means that \underline{there cannot be spaces}
	in the user's input. Using \texttt{cin} and the stream operator this way
	means that it stops reading once it hits \textbf{whitespace} (spaces, newline,
	tabs, etc.)
	
	\begin{center}
	\textbf{It is fine to use \texttt{cin >>} for this lab. \\
	Using \texttt{getline()}, as described below, is optional.}
	\end{center}
	
\begin{hint}{Getting a full line of text (optional)}
	If you want to get spaces in the user's input, you have to use the \texttt{getline()} function,
	like this:

\begin{lstlisting}[style=code]
getline( cin, hometown );
\end{lstlisting}

	You have to give the \texttt{getline()} function two items:
	the \texttt{cin} object, and the string to store the result in, \texttt{hometown}.
	
	~\\
	\color{red}The buffer problem:\color{black} \tab
	However, there is a problem that arises when mixing \texttt{cin >>} and
	\texttt{getline()}! Because of how the \textbf{input buffer} works, if you
	do a \texttt{cin >>} command and then a \texttt{getline()} command,
	your getline() won't trigger because it's getting data from the buffer.
	In other words, the program won't stop to get the user's input.
	
	~\\
	The \textbf{solution} is to add \texttt{cin.ignore();} \textit{before}
	your \texttt{getline()} statement, but \underline{only} if it was preceeded by
	a \texttt{cin >>} command.
	
\begin{lstlisting}[style=code]
cin >> a;
cin.ignore();
getline( cin, b );
\end{lstlisting}

	In \texttt{main()}, I use \texttt{cin >>} to get the user's menu
	selection, which will cause this problem to occur. So, you will need
	to use \texttt{cin.ignore();} only \underline{before the first time} you use
	\texttt{getline()}.

\end{hint}
	
	
	\paragraph{Testing:} ~\\
	Make sure to build, run, and test the program. You will need to test three times for proper coverage:
	
	\begin{center}
		\begin{tabular}{p{1cm} p{6cm} p{5cm}}
			\textbf{Test case} 	& \textbf{Input} 							& \textbf{Expected output} \\ \hline
			1					& A hometown with 7+ characters				& ``That's a long name!'' is displayed.
			\\ \hline
			2					& A hometown with 6 characters 				& ``That's a long name!'' is \textbf{not} displayed.
			\\ \hline
			3					& A hometown with fewer than 6 characters 	& ``That's a long name!'' is \textbf{not} displayed.
		\end{tabular}
	\end{center}
	
~\\ Long hometown:
\begin{lstlisting}[style=output]
Enter your hometown: OverlandPark
That's a long name!
Enter your name: JCCC
Hello JCCC from OverlandPark!
\end{lstlisting}
~\\ Short hometown:
\begin{lstlisting}[style=output]
Enter your hometown: OP
Enter your name: JCCC
Hello JCCC from OverlandPark!
\end{lstlisting}
	
	
	
	
	
	
	
	
	
	
	

\newpage
\subsection*{Program 2: Pass/fail (if / else statements)}

	\begin{itemize}
		\item	This program will ask the user for the \# of points they earned on an assignment,
				and the total possible point value.
		\item	It will do simple math to figure out the grade \% for the assignment.
		\item	Based on the score, they will either get a \textit{pass} (60\% or higher) or \textit{fail} (below 60\%) grade.
	\end{itemize}
	
	\paragraph{Variables:}
	
	\begin{center}	
		\begin{tabular}{l l l}
			\textbf{Variable name} 	& \textbf{Data type}	& \textbf{Description} \\ \hline
			\texttt{userPoints}		& \texttt{float}		& The amount of points the user got \\
			\texttt{totalPoints}	& \texttt{float}		& The total points possible for the assignment \\
			\texttt{grade}			& \texttt{float}		& Their \% grade.
		\end{tabular}
	\end{center}
	
	\begin{hint}{How do I calculate their grade?}
		Their grade will be \texttt{ userPoints / totalPoints } to get the decimal
		version of their grade. You can multiply it by 100 to get a percentage.
		~\\
		\texttt{grade = (userPoints / totalPoints) * 100}
	\end{hint}
	
	\begin{hint}{What about integers?}
		You could make the \texttt{userPoints} and \texttt{totalPoints} variables
		integers if you wanted to. However, when you do division between two integers
		in C++, the \textbf{result will also be an integer} - meaning that you lose
		the decimal portion. You would have to \textbf{cast} (convert) one or both values to 
		a float in order to get a float result.
		~\\
		\texttt{grade = ((float)userPoints / totalPoints) * 100}
	\end{hint}
	
	\newpage
	\paragraph{Testing:} ~\\
	Make sure to build, run, and test the program. You will need to test three times:
	
	\begin{center}
		\begin{tabular}{p{1cm} p{6cm} p{5cm}}
			\textbf{Test case} 	& \textbf{Input} 							& \textbf{Expected output} \\ \hline
			1					& Something that gives a 60\% grade				& ``You passed!'' is displayed.
			\\					& e.g., 30 out of 50 points
			\\ \hline
			2					& Something that gives above a 60\% grade		& ``You passed!'' is displayed.
			\\					& e.g., 80 out of 100 points
			\\ \hline
			3					& Something that gives below a 60\% grade		& ``You failed!'' is \textbf{not} displayed.
			\\					& e.g., 25 out of 50 points
		\end{tabular}
	\end{center}
	
~\\ Pass grade:
\begin{lstlisting}[style=output]
How many points does the assignment have?   40
How many points did you get?                38
Score:                                      0.95
You passed!
\end{lstlisting}

~\\ Fail grade:
\begin{lstlisting}[style=output]
How many points does the assignment have?   50
How many points did you get?                20
Score:                                      0.4
You failed!
\end{lstlisting}

	\begin{hint}{Formatting output?}
		Your output doesn't need to match mine 100\%. To get the formatting
		I did, I just added extra spaces in my \texttt{cout} statements,
		or used the \texttt{"\textbackslash t"} character to to tab things
		over to look nice.
	\end{hint}





















\newpage
\subsection*{Program 3: Battery charge (if / else if statements)}

	\begin{itemize}
		\item	For this program, it will ask what \% of charge is left in the user's phone battery.
		\item	Based on the \% of charge left, it will draw a picture of a battery and its charge.
	\end{itemize}
	
	\paragraph{Variables:} ~\\
	
	\begin{center}	
		\begin{tabular}{l l l}
			\textbf{Variable name} 	& \textbf{Data type}	& \textbf{Description} \\ \hline
			\texttt{charge}			& \texttt{int}			& The charge \% \\
		\end{tabular}
	\end{center}
	
	\begin{hint}{Percentages}
		When the user enters a percent, they \textbf{should not write \%} in their input -
		integers can only store whole numbers, no special symbols.
	\end{hint}
	
	\begin{center}
		\begin{tabular}{ l l l }
			\textbf{Graphic} 	& \textbf{Charge level:} & \textbf{Range:} \\ \hline
			\texttt{ [****] }	& Full charge 	& [75,100] \\
			\texttt{ [***\_] }	& 3/4th charge 	& [50, 75) \\
			\texttt{ [**\_\_] }	& 1/2 charge  	& [25, 50) \\
			\texttt{ [*\_\_\_] }	& 1/4 charge 	& [5, 25) \\
			\texttt{ [\_\_\_\_] }	& No charge  	& [0, 5) \\
		\end{tabular}
		
		[ ] = inclusive, ( ) = exclusive
	\end{center}
	

	\newpage
	\paragraph{Testing:} ~\\
	Make sure to build, run, and test the program.
	How many test cases are needed to fully test? I would make a test case to check
	each range edge (75 should be full, 50 should be 3/4th, 25 should be 1/2),
	and one number per range that is somewhere in-between. I would check the edges of the
	ranges to make sure that you're using $>$, $\geq$, etc. appropriately.
	
	\begin{center}
		\begin{tabular}{p{1cm} p{6cm} p{5cm}}
			\textbf{Test case} 	& \textbf{Input} 				& \textbf{Expected output} \\ \hline
			1					& charge = 80					& \texttt{ [****] } \\ \hline
			2					& charge = 75 (edge)			& \texttt{ [****] } \\ \hline
			
			3					& charge = 60					& \texttt{ [***\_] } \\ \hline
			4					& charge = 50 (edge)			& \texttt{ [***\_] } \\ \hline
		
			5					& charge = 30					& \texttt{ [**\_\_] } \\ \hline
			6					& charge = 25 (edge)			& \texttt{ [**\_\_] } \\ \hline
		
			7					& charge = 20					& \texttt{ [*\_\_\_] } \\ \hline
			8					& charge = 5 (edge)				& \texttt{ [*\_\_\_] } \\ \hline
			
			9					& charge = 4					& \texttt{ [\_\_\_\_] } \\ \hline
			10					& charge = 0 (edge)				& \texttt{ [\_\_\_\_] } \\ \hline
		\end{tabular}
	\end{center}
	
~\\ Example outputs:
\begin{lstlisting}[style=output]
Enter your phone charge: 80
[****]
\end{lstlisting}

\begin{lstlisting}[style=output]
Enter your phone charge: 60
[***_]
\end{lstlisting}

\begin{lstlisting}[style=output]
Enter your phone charge: 35
[**__]
\end{lstlisting}

\begin{lstlisting}[style=output]
Enter your phone charge: 20
[*___]
\end{lstlisting}

\begin{lstlisting}[style=output]
Enter your phone charge: 2
[____]
\end{lstlisting}




















\newpage
\subsection*{Program 4: Input validation (logic operators)}



	\begin{itemize}
		\item	For this program, the user will have a list of four options to choose from.
				(You can customize the menu however you'd like).
		\item	We will use an if statement with logic operators (AND: \texttt{\&\&}, OR: \texttt{||})
				to validate their choice.
		\item	Display ``Good choice!'' if they selected a valid option, or
				``Invalid choice!'' if they chose something outside of the range.
	\end{itemize}
	
	\paragraph{Variables:} ~\\
	
	\begin{center}	
		\begin{tabular}{l l l}
			\textbf{Variable name} 	& \textbf{Data type}	& \textbf{Description} \\ \hline
			\texttt{userChoice}		& \texttt{int}		& The user's choice \\
		\end{tabular}
	\end{center}
	
	\paragraph{Example output:} ~\\
\begin{lstlisting}[style=output]
What is your favorite type of book?
1. Scifi
2. Historical
3. Fantasy
4. DIY

Your selection: 2
Good choice!
\end{lstlisting}

\begin{lstlisting}[style=output]
What is your favorite type of book?
1. Scifi
2. Historical
3. Fantasy
4. DIY

Your selection: 5
Invalid choice!
\end{lstlisting}

	\newpage
	\paragraph{What is valid/invalid?} ~\\
	
	There are multiple ways you could approach checking the validity
	of the user's input. You could check to see if the input is
	1, 2, 3, or 4, or you could check with ranges, you could be checking
	for ``if valid... else...'' or ``if invalid... else...''
	So long as it's logically sound, any of these approaches are fine.

	\begin{itemize}
		\item	Is valid: You could look at if
				\texttt{userChoice} is 1, \underline{or} 
				\texttt{userChoice} is 2, \underline{or} 
				\texttt{userChoice} is 3, \underline{or} 
				\texttt{userChoice} is 4.
				~\\ ~\\
				\texttt{ userChoice == 1 || userChoice == 2 || \\userChoice == 3 || userChoice == 4 }
		\item	Is valid: You could check to make sure \texttt{userChoice} is
				1 or greater, \underline{and} \texttt{userChoice} is 4 or less.
				~\\ ~\\
				\texttt{ userChoice >= 1 \&\& userChoice <= 4 }
				
		\item	Is invalid: You chould check if \texttt{userChoice} is not any of the valid options.
				~\\ ~\\
				\texttt{ userChoice != 1 \&\& userChoice != 2 \&\& \\ userChoice != 3 \&\& userChoice != 4 }
		
		\item	Is invalid: You could check if \texttt{userChoice} is outside the valid range, on either side.
				~\\ ~\\
				\texttt{ userChoice < 1 || userChoice > 4 }
	\end{itemize}
	
	\begin{hint}{What's the best?}
		I tend to write my if statements so that the \textbf{if} clause is something positive -
		not ``if not true'' or ``if false'', but ``if true...'', so the first thing I'd check is:
		\begin{itemize}
			\item	If input is valid then...
			\item	Else (it's invalid) do other things...
		\end{itemize}
		
		And since the menu has several values (and could even be expanded upon!)
		I would use ranges to check for valid values.
	\end{hint}
	

	\newpage
	\paragraph{Testing:} ~\\
	Make sure to build, run, and test the program.
	~\\ ~\\
	Test coverage should account for all four valid options, plus something outside of the range
	less than 1, and something outside of the range greater than 4.
	
	\begin{center}
		\begin{tabular}{p{1cm} p{3cm} p{9cm}}
			\textbf{Test case} 	& \textbf{Input} 		& \textbf{Expected output} \\ \hline
			1					& 1						& ``Good choice!'' is displayed. \\ \hline
			2					& 2						& ``Good choice!'' is displayed. \\ \hline
			3					& 3						& ``Good choice!'' is displayed. \\ \hline
			4					& 4						& ``Good choice!'' is displayed. \\ \hline
			5					& 0						& ``Invalid choice!'' is displayed. \\ \hline
			6					& 5						& ``Invalid choice!'' is displayed. \\ \hline
		\end{tabular}
	\end{center}
	
~\\ Valid choice:
\begin{lstlisting}[style=output]
What is your favorite type of book?
1. Scifi
2. Historical
3. Fantasy
4. DIY

Your selection: 2
Good choice!
\end{lstlisting}
	
~\\ Invalid choice:
\begin{lstlisting}[style=output]
Your selection: -5
Invalid choice!
\end{lstlisting}
	
~\\ Invalid choice:
\begin{lstlisting}[style=output]
Your selection: 100
Invalid choice!
\end{lstlisting}

















\newpage
\subsection*{Program 5: Switch statements}

	\begin{itemize}
		\item	This program will get two numbers (as floats) from the user.
		\item	It will also ask the user whether they want to do addition, subtraction, division, or multiplication.
		\item	It will use a switch statement to figure out what math operation to use.
		\item	If the user didn't enter +, -, *, or / for the operation, it will display an error message.
	\end{itemize}
	
	\paragraph{Variables:} ~\\
	
	\begin{center}	
		\begin{tabular}{l l l}
			\textbf{Variable name} 	& \textbf{Data type}	& \textbf{Description} \\ \hline
			\texttt{num1}			& \texttt{float}		& The first number \\
			\texttt{num2}			& \texttt{float}		& The second number \\
			\texttt{result}			& \texttt{float}		& The result of the math operation \\
			\texttt{operator}		& \texttt{char}			& Which math operator \\
		\end{tabular}
	\end{center}
	
	\paragraph{Example output:} ~\\
\begin{lstlisting}[style=output]
Enter first number:		3
Enter second number: 	5

Which type of operation?
 +: add
 -: subtract 
 *: multiply 
 /: divide
	
Choice: +

Result: 8
\end{lstlisting}

	\paragraph{Switch statement:} ~\\
	A switch statement only checks if some variable is equal to one of the
	\textbf{case} values, which can be good for menus like these.
	
	\newpage
	A switch statement looks like this:
\begin{lstlisting}[style=code]
switch( VARIABLE )
{	
	case VAL1:		// if VARIABLE == VAL1
	break;
	
	case VAL2:		// if VARIABLE == VAL2
	break;
	
	default:		// else
}
\end{lstlisting}

	You can build out a switch statement that checks the value of \texttt{operator},
	and use \texttt{case '+':} to check if operator was set to the plus sign.
	
	~\\
	Within the case, you would just write an assignment statement, adding \texttt{num1}
	to \texttt{num2} and storing the result in \texttt{result}.
	
	~\\
	After the switch statement, use \texttt{cout} to display the result.

	\paragraph{Division by zero?} ~\\
	Your program might crash if the user enters 0 for \texttt{num2} and
	then decides to do division. You could use an if statement within your
	switch statement to prevent division by zero. If you prevent
	the division by 0, then \texttt{result} doesn't get assigned to,
	and will have garbage stored in it. (This is OK, or you can come up
	with a nicer looking result.)
	
\begin{lstlisting}[style=output]
Enter first number: 10
Enter second number: 0
What type of operation?
 +: add
 -: subtract
 *: multiply
 /: divide

Choice: /
Can't divide by 0!

Result: 4.59149e-41
\end{lstlisting}

	\newpage
	\paragraph{Testing:} ~\\
	Make sure to build, run, and test the program.
	
	~\\
	You should have a test for each operation, plus any error checks you want to do.
	
	\begin{center}
		\begin{tabular}{p{1cm} p{7cm} p{4cm}}
			\textbf{Test case} 	& \textbf{Input} 							& \textbf{Expected output} \\ \hline
			1					& num1 = 3, num2 = 5, operation = '+'		& 8 \\ \hline
			2					& num1 = 3, num2 = 5, operation = '-'		& -2 \\ \hline
			3					& num1 = 3, num2 = 5, operation = '*'		& 15 \\ \hline
			4					& num1 = 3, num2 = 5, operation = '/'		& 0.6 \\ \hline
			5					& num1 = 3, num2 = 0, operation = '/'		& ``Invalid operation!'' \\ \hline
			6					& num1 = 1, num2 = 1, operation = '\$'		& ``Invalid operation!'' \\ \hline
		\end{tabular}
	\end{center}

~\\
Example output:
\begin{lstlisting}[style=output]
Enter first number: 1
Enter second number: 1
What type of operation?
 +: add
 -: subtract
 *: multiply
 /: divide

Choice: @
Invalid operation!
\end{lstlisting}

\begin{lstlisting}[style=output]
Enter first number: 2
Enter second number: 3
What type of operation?
 +: add
 -: subtract
 *: multiply
 /: divide

Choice: -

Result: -1
\end{lstlisting}


\end{document}

