#include <iostream>
#include <iomanip>
using namespace std;

int main(){
    cout << "DISTANCE" << endl << endl;

    cout << "First coordinate pair,  enter x and y: 0 0" << endl;
    cout << "Second coordinate pair, enter x and y: 5 1" << endl;
    cout << "Distance: 5.09902" << endl;
    cout << endl;
    cout << "First coordinate pair,  enter x and y: 1 2" << endl;
    cout << "Second coordinate pair, enter x and y: 3 4" << endl;
    cout << "Distance: 2.82843" << endl;

    return 0;
}
