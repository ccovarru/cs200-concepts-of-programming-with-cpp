\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Tools setup}
\newcommand{\laTitle}       {CS 200 Lab}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ / \laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

\togglefalse{answerkey}

\section*{Lab instructions}

	\begin{center}
		\includegraphics[width=12cm]{images/lab00-codeblocks-sourcelocation.png}
	\end{center}
	
\begin{itemize}
	\item	Once you've finished the lab, turn in your \textbf{.cpp} source file in on Canvas.
			In Windows, they will be marked as a ``C++ source file'' type.
	\item	I prefer files to \textbf{not be zipped}, unless otherwise stated.
	\item	\textbf{Do not} zip an entire project/solution directory. I don't want all the
			extra files. One source file is a few bytes; the entire directory will be several
			megabytes and take longer to download.
\end{itemize}

\newpage
\section*{Setting up your IDE}

\begin{intro}{What is an IDE again?}
	An IDE is an Integrated Development Environment.
	IDEs contain multiple types of tools that help you
	effectively create software in C++, such as a
	\textbf{text editor with syntax highlighting},
	a \textbf{compiler} to turn source into executables,
	and a \textbf{debugger} to help you debug your code.
\end{intro}

You can write C++ code on any operating system; C++ is not a product
of a single company (like Microsoft), so there are a lot of options
out there for tools you can use to write C++ in. In general, my suggestions
would be:

\begin{center}
\begin{tabular}{l c c c c}
					& Windows 	& Linux 	& Mac 	& Slow computer
	\\ \hline
	Visual Studio	& x
	\\
	Code::Blocks	& x 		& x			& x		& x
	\\
	Xcode			&			&			& x
\end{tabular}
\end{center}

\begin{itemize}
	\item	\textbf{Visual Studio...}  https://visualstudio.microsoft.com/vs/community/
	\begin{itemize}
		\item	A very common tool, being used
				on JCCC computers in the lab, at UMKC, and at lots of
				software development companies here in Kansas City.
		\item	It can also be very bloated and very slow. If your computer
				is having trouble running Visual Studio, you might try
				Code::Blocks instead.
	\end{itemize} 
	
	\item	\textbf{Code::Blocks...} http://www.codeblocks.org/downloads/26
	\begin{itemize}
		\item	Runs on Windows, Linux and Mac.
		\item	Is lighter-weight than Visual Studio.
		\item	Has the same debugging tools needed.
		\item	I've never worked at a company that uses Code::Blocks,
				but I prefer it when writing C++ for myself.
		\item	When downloading on Windows, make sure to get the version
				that says \textbf{``mingw-setup.exe''!}
	\end{itemize}
	
	\item	\textbf{Xcode...} https://developer.apple.com/xcode/
	\begin{itemize}
		\item	Runs on Mac.
		\item	Has the same debugging tools needed.
		\item	I am not very familiar with it.
		\item	Also used professionally a lot.
	\end{itemize}
\end{itemize}

\tableofcontents

\newpage
\section{Using Visual Studio}
\subsection{Downloading Visual Studio}

	From the Visual Studio Community page, select \textbf{Download Visual Studio}.
	~\\
	(https://visualstudio.microsoft.com/vs/community/)
	
	\begin{center}
		\includegraphics[width=12cm]{images/lab00-visualstudio-download.png}
	\end{center}
	
	Open the installer and it will begin downloading all the files it needs.
	This will take a while.
	
	\begin{center}
		\includegraphics[width=12cm]{images/lab00-visualstudio-download2.png}
	\end{center}
	
\newpage
\subsection{Installing Visual Studio}

	Once the installer is ready, it will pop up a window with different
	toolsets that Visual Studio supports.
	Make sure to locate \textbf{``Desktop development with C++''},
	then click \textbf{Install}.

	\begin{center}
		\includegraphics[width=14cm]{images/lab00-visualstudio-install1.png}
	\end{center}
	
	\begin{center}
		\includegraphics[width=12cm]{images/lab00-visualstudio-install1b.png}
	\end{center}
	
	It will then take a while to install, and request that you restart your
	computer after it's done installing. Go ahead and do that before continuing.
	
	When you first launch Visual Studio, it will ask you to log in with a Microsoft account.
	This could be a hotmail account or something. I have a hotmail account that I only
	use to log into Visual Studio. You can skip this step, but I am not sure if it makes
	you log in later on in the future.

\newpage
\subsection{Creating a program}

	In Visual Studio, we create \textbf{solutions} for a program.
	Later on, programs will have multiple source code files, but
	for now we will just have one source file.
	
	When Visual Studio is open, there's a splash screen with some
	common options. Select \textbf{Create a new project}.
	
	\begin{center}
		\includegraphics[width=10cm]{images/lab00-visualstudio-makeproject1.png}
	\end{center}
	
	Then select \textbf{Empty Project} and click \textbf{Next}.
	
	\begin{center}
		\includegraphics[width=12cm]{images/lab00-visualstudio-makeproject2.png}
	\end{center}
	
	Next, you need to set the \textbf{Project name} and the \textbf{Location}.
	I would suggest making one directory for your CS 200 projects, like
	
\begin{verbatim}
C:\CS200\
\end{verbatim}

	instead of the default path that Visual Studio provides.
	
	\begin{center}
		\includegraphics[width=12cm]{images/lab00-visualstudio-makeproject3.png}
	\end{center}
	
	Click \textbf{Create} once you're done here.

\newpage

	Now we have an empty solution. We need to add one source code file
	to get started with our program. On the right-hand side of the screen
	is the \textbf{Solution Explorer} with the solution (``Solution ToolsSetup'')
	and the project (``ToolsSetup'').
	
	Right-click on the project and click on Add $\to$ New item...
	\begin{center}
		\includegraphics[width=14cm]{images/lab00-visualstudio-newfile1.png}
	\end{center} 
	
	\newpage
	Select the \textbf{C++ File (.cpp)} and give it a name like \textbf{tools.cpp}

	\begin{center}
	\color{red}NOTE: the filename MUST end in .cpp!\color{black}
	\end{center}
	
	\begin{center}
		\includegraphics[width=12cm]{images/lab00-visualstudio-newfile2.png}
	\end{center} 
	
	\begin{center}
		\includegraphics[width=12cm]{images/lab00-visualstudio-newfile3.png}
	\end{center} 
	
	Click on \textbf{Add} once you're done.
	
	Now in your \textbf{Solution Explorer} you'll see your \textbf{tools.cpp}
	file under ``Source Files'', and the editor should have opened the source
	file as well, allowing you to start typing code.

\newpage
\subsection{Writing a program}


	Type out the following program source code. Note that copy-paste
	out of this document might not work properly, so it's better to
	just type it all out.

\begin{lstlisting}[style=code]
#include <iostream>
using namespace std;

int main()
{
    cout << "Hello, world!" << endl;
    return 0;
}
\end{lstlisting}

	This source code does the following things:
	
	\footnotesize
	\begin{enumerate}
		\item	include iostream: Add the console input/output library so we can output text to the screen.
		\item	using namespace std: Marks that we're using the standard C++ namespace, so we don't have to type ``std::cout'' whenever we want to output.
		\item	int main(): the start of our program.
		\item	cout: Outputs the text ``Hello, world'' to the screen.
		\item	return 0: ends the program.
	\end{enumerate}
	\normalsize
	
	It's OK if this stuff doesn't make total sense yet; you will be learning
	more about all of these things this semester. Sometimes, at first,
	you just have to take certain things for granted. :)
	
	

\newpage
\subsection{Building and running the program}

	Next, we need to \textbf{build and run} the program to make sure
	that the code works!
	
	In the dropdown menu at the top of the screen, click on
	Build $\to$ Build Solution.

	\begin{center}
		\includegraphics[width=12cm]{images/lab00-visualstudio-build1.png}
	\end{center} 
	
	At the bottom, you'll see an \textbf{Output} window, and if there
	are no errors in the code, you'll see a message like this:

	\begin{center}
		\includegraphics[width=14cm]{images/lab00-visualstudio-build2.png}
	\end{center}
	
	If there ARE errors, the \textbf{Error List} will show up, and you can click
	on each error in the list and Visual Studio will show you the line of
	code that it \textit{thinks} is causing the problem.

	\begin{center}
		\includegraphics[width=14cm]{images/lab00-visualstudio-build3.png}
	\end{center}
	
	\newpage
	Once the errors are taken care of, you can click on the toolbar
	button with the ``play'' arrow that says \textbf{Local Windows Debugger}
	to start the program.

	\begin{center}
		\includegraphics[width=8cm]{images/lab00-visualstudio-build4.png}
	\end{center}
	
	It will pop up a \textbf{console window} running your program.
	
	\begin{center}
		\includegraphics[width=10cm]{images/lab00-visualstudio-build5.png}
	\end{center}
	
	It also has some extra information at the end of the program - this
	is just some debug information to let you know that the program
	executed correctly.
	
	You can then press \textbf{Enter} to close the program.
	
\newpage
\subsection{Locating your source file}

	When you were creating the solution for your project in Visual Studio,
	one of the fields to fill out was the location of your project.
	
	\begin{center}
		\includegraphics[width=12cm]{images/lab00-visualstudio-makeproject3.png}
	\end{center}
	
	On your computer, navigate to the folder you had set. There will be
	a folder automatically generated for your project.
	
	\begin{center}
		\includegraphics[width=8cm]{images/lab00-visualstudio-locate.png}
	\end{center}

	Open this folder.
	
	\newpage
	This first directory contains the \textbf{Solution (.sln)} file,
	which is what you will open if you want to start working on the same
	project again.
	
	Within this folder, there should be a second \textbf{ToolsSetup} folder -
	open this one up as well.
	
	\begin{center}
		\includegraphics[width=14cm]{images/lab00-visualstudio-locate2.png}
	\end{center}
	
	Within this second folder, there will be your source files.
	Your Windows might not be set up to see file extensions (this is an
	option in the Folder Options settings), but the Type of the source 
	file is ``C++ source file''. This will be the file that you upload
	on Canvas.
	
	\begin{center}
		\includegraphics[width=14cm]{images/lab00-visualstudio-locate3.png}
	\end{center}


\newpage
\section{Using Code::Blocks}
\subsection{Downloading Code::Blocks}

	Go to \textbf{codeblocks.org} and click on the \textbf{Downloads} tab
	in the navbar of the webpage. Next, select \textbf{Download the binary release}
	(a ``binary'' is another term for an executable file).
	~\\
	(http://www.codeblocks.org/downloads/26)
	
	\begin{center}
		\includegraphics[width=12cm]{images/lab00-codeblocks-download1.png}
	\end{center}
	
	If you're on a Windows machine, make sure to download the version of
	Code::Blocks with \textbf{mingw-setup} in the title.
	
	\begin{center}
		\includegraphics[width=10cm]{images/lab00-codeblocks-download2.png}
	\end{center}
	
	There are two links to the right: one to download via \textbf{FossHUB}
	and one to download via \textbf{Sourceforge.net}. These are both places
	where Free/Open Source Software can be hosted, so select either of these to
	download the Code::Blocks installer.

\subsection{Installing Code::Blocks}
	
	Once the installer is downloaded, run it and go through the install options.
	Everything can be left as the default values, unless you want to change
	something like the install directory.

\subsection{Creating a program}

	In Code::Blocks, we create \textbf{projects} for a program.
	Later on, programs will have multiple source code files, but
	for now we will just have one source file.

	Once you open up Code::Blocks, there will be a main view and
	a link to create a new project:
	
	\begin{center}
		\includegraphics[width=6cm]{images/lab00-codeblocks-newproject1.png}
	\end{center}
	
	Or, you can go to the top dropdown menu, selecting
	File $\to$ New $\to$ Project...
	
	\begin{center}
		\includegraphics[width=12cm]{images/lab00-codeblocks-newproject1b.png}
	\end{center}
	
	\newpage
	\paragraph{Project type selection screen:}
	
	Next, you select what kind of project you want to make. Select
	\textbf{Empty project} and then click \textbf{Go}.
	
	\begin{center}
		\includegraphics[width=10cm]{images/lab00-codeblocks-newproject2.png}
	\end{center}
	
	\paragraph{Project information screen:} 
	On the next screen, you will need to text a \textbf{Project title}
	and the \textbf{Folder to create project in}. The other fields will
	be set up automatically. I would suggest that you keep all your CS 200
	assignments in one folder.
	
	\begin{center}
		\includegraphics[width=10cm]{images/lab00-codeblocks-newproject3.png}
		
		\footnotesize
		Note: I'm in Linux so I don't have a ``C:'' directory.
		\normalsize
	\end{center}
	
	\newpage
	\paragraph{Compiler information screen:}
	
	On this screen, keep all the defaults and just click \textbf{Finish}.
	
	\begin{center}
		\includegraphics[width=9cm]{images/lab00-codeblocks-newproject4.png}
	\end{center}

	Now we have an empty project!
	
	\begin{center}
		\includegraphics[width=8cm]{images/lab00-codeblocks-emptyproject.png}
	\end{center}
	
	\newpage
	Next, we will need to add a \textbf{source file} so that we can
	begin writing some C++.
	Go to the top dropdown menu again and select
	File $\to$ New $\to$ Empty file.

	\begin{center}
		\includegraphics[width=12cm]{images/lab00-codeblocks-newfile1.png}
	\end{center}
	
	It will ask, ``Do you want to add this new tile in the active project (has to be saved first)?''
	and select \textbf{Yes}.
	
	Give the file a name like \textbf{tools.cpp}.
	
	\begin{center}
	\color{red}NOTE: the filename MUST end in .cpp!\color{black}
	\end{center}

	\begin{center}
		\includegraphics[width=12cm]{images/lab00-codeblocks-newfile2.png}
	\end{center}
	
	After saving, it will ask if the file should be added for Debug and/or Release
	targets (basically, if you wanted, you could write code just for ``debug mode'').
	These should both be checked by default. Then hit \textbf{OK}.

	\begin{center}
		\includegraphics[width=8cm]{images/lab00-codeblocks-newfile3.png}
	\end{center}

	On the left side view in Code::Blocks, you will see the \textbf{Projects}
	tab. When you double-click on your project now, you will see
	your source file below it. Double-click on the .cpp file to open it.

	\begin{center}
		\includegraphics[width=8cm]{images/lab00-codeblocks-newfile4.png}
	\end{center}
	
	Now we're ready to add some code!
	
\newpage
\subsection{Writing a program}

	Type out the following program source code. Note that copy-paste
	out of this document might not work properly, so it's better to
	just type it all out.

\begin{lstlisting}[style=code]
#include <iostream>
using namespace std;

int main()
{
    cout << "Hello, world!" << endl;
    return 0;
}
\end{lstlisting}

	This source code does the following things:
	
	\footnotesize
	\begin{enumerate}
		\item	include iostream: Add the console input/output library so we can output text to the screen.
		\item	using namespace std: Marks that we're using the standard C++ namespace, so we don't have to type ``std::cout'' whenever we want to output.
		\item	int main(): the start of our program.
		\item	cout: Outputs the text ``Hello, world'' to the screen.
		\item	return 0: ends the program.
	\end{enumerate}
	\normalsize
	
	It's OK if this stuff doesn't make total sense yet; you will be learning
	more about all of these things this semester. Sometimes, at first,
	you just have to take certain things for granted. :)

\newpage
\subsection{Building and running the program}

	Next, we need to \textbf{build and run} the program to make sure
	that the code works!
	
	You can build by going to the dropdown menu and selecting
	Build $\to$ Build, or you can click the little gear
	icon in the toolbar.
	
	\begin{center}
		\begin{tabular}{ c c c }
			\includegraphics[width=6cm]{images/lab00-codeblocks-build1.png}
			&
			\includegraphics[width=1cm]{images/lab00-codeblocks-build1b.png}
			&
			\texttt{Ctrl+F9}
			\\
			Build $\to$ Build & Build icon & Keyboard shortcut
		\end{tabular}
	\end{center}
	
	The program will build, and there will be information that shows up
	under the \textbf{Build log} tab, which should be at the bottom
	of your Code::Blocks screen.

	\begin{center}
		\includegraphics[width=13cm]{images/lab00-codeblocks-build2.png}
	\end{center}
	
	The screenshot above is what it looks like when the build is OK - 
	no errors! 
	If there are any problems building your program, they will show up
	in here as well.

	\begin{center}
		\includegraphics[width=13cm]{images/lab00-codeblocks-build3.png}
	\end{center}
	
	If this happens, you can click on the error and it will take you to
	the line of code it \textit{thinks} is the problem. In this case,
	I had purposefully misspelled ``cout'' as ``coutt'', causing
	a \textbf{syntax error} that the compiler found for me.
	
	\newpage
	Once everything has built fine, you can \textbf{run} the program.
	This is under Build $\to$ Run, or you can click on the ``play''
	icon in the toolbar.
	
	\begin{center}
		\begin{tabular}{ c c c }
			\includegraphics[width=6cm]{images/lab00-codeblocks-build4.png}
			&
			\includegraphics[width=1cm]{images/lab00-codeblocks-build4b.png}
			& 
			\texttt{Ctrl+F10}
			\\
			Build $\to$ Run & Run icon & Keyboard shortcut
		\end{tabular}
	\end{center}
	
	Or, in the future, you can just click on ``Build and run'', which will
	automatically run the program if there are no compile errors.
	
	\begin{center}
		\begin{tabular}{ c c c }
			\includegraphics[width=6cm]{images/lab00-codeblocks-build5.png}
			&
			\includegraphics[width=1cm]{images/lab00-codeblocks-build5b.png}
			& 
			\texttt{F9}
			\\
			Build $\to$ Build and run & Build and run icon & Keyboard shortcut
		\end{tabular}
	\end{center}
	
	Once everything is built and run is triggered, a \textbf{console window}
	will pop up with your program running.

	\begin{center}
		\includegraphics[width=13cm]{images/lab00-codeblocks-build6.png}
	\end{center}	
	
	It also has some extra information at the end of the program - this
	is just some debug information to let you know that the program
	executed correctly.
	
	You can then press \textbf{Enter} to close the program.	

\newpage
\subsection{Locating your source file}

	When you first created your project in Code::Blocks, there was a directory
	it had asked for.

	\begin{center}
		\includegraphics[width=10cm]{images/lab00-codeblocks-newproject3b.png}
	\end{center}
	
	On your computer, navigate to the folder you had set, and there will
	be a folder automatically made for each project you create.
	
	\begin{center}
		\includegraphics[width=12cm]{images/lab00-codeblocks-sourcelocation.png}
	\end{center}
	
	On your system, you might not have \textbf{show file extensions} set to ``on''
	(you can do this through Folder Options in Windows), but make sure you're
	finding the file marked as a ``C++ source file'' type.	
	Make sure you just turn in \textbf{cpp source files}; I don't need any
	project files (.cbp files).
\end{document}









