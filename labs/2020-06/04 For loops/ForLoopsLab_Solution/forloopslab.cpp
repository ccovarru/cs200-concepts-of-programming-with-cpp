#include <iostream>
#include <string>
using namespace std;

void Program1()
{
}

void Program2()
{
}

void Program3()
{
}

void Program4()
{
    string text;
    char lookForLetter;
    int letterCount;

    cout << "Enter a string: ";
    cin.ignore();
    getline( cin, text );

    cout << "Enter a letter to count: ";
    cin >> lookForLetter;

    letterCount = 0;

    for ( int i = 0; i < text.size(); i++ )
    {
        cout << "Letter " << i << ": " << text[i] << endl;

        if ( tolower(text[i]) == tolower(lookForLetter) )
        {
            letterCount++;
        }
    }

    cout << endl << "There are " << letterCount << " "
        << lookForLetter << "(s) in \"" << text << "\"" << endl;
}

int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-4): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
