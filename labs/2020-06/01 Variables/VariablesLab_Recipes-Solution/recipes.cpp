#include <iostream>
using namespace std;

int main()
{
    // 1. Declare ingredients variables
    float cupsOfButter = 1;
    float cupsOfSugar = 1;
    float eggs = 2;
    float tspsVanilla = 2;
    float tspsBakingSoda = 1;
    float tspsSalt = 0.5;
    float cupsFlour = 3;
    float cupsChocolateChips = 2;

    // 2. Display ingredients to the screen
    cout << endl << "INGREDIENTS LIST" << endl;
    cout << cupsOfButter << " cups of butter" << endl;
    cout << cupsOfSugar << " cups of sugar" << endl;
    cout << eggs << " eggs" << endl;
    cout << tspsVanilla << " teaspoons vanilla" << endl;
    cout << tspsBakingSoda << " teaspoons baking soda" << endl;
    cout << tspsSalt << " teaspoons salt" << endl;
    cout << cupsFlour << " cups of flour" << endl;
    cout << cupsChocolateChips << " cups of chocolate chips" << endl;

    // 3. Create a variable for "batchSize"
    float batchSize;

    // 4. Ask the user how many batches to make
    cout << endl << endl;
    cout << "How many batches? ";
    cin >> batchSize;

    // 5. Recalculate ingredient amounts
    cupsOfButter *= batchSize;
    cupsOfSugar *= batchSize;
    eggs *= batchSize;
    tspsVanilla *= batchSize;
    tspsBakingSoda *= batchSize;
    tspsSalt *= batchSize;
    cupsFlour *= batchSize;
    cupsChocolateChips *= batchSize;

    // 6. Display updated ingredient list
    cout << endl << "ADJUSTED INGREDIENTS LIST" << endl;
    cout << cupsOfButter << " cups of butter" << endl;
    cout << cupsOfButter << " cups of sugar" << endl;
    cout << eggs << " eggs" << endl;
    cout << tspsVanilla << " teaspoons vanilla" << endl;
    cout << tspsBakingSoda << " teaspoons baking soda" << endl;
    cout << tspsSalt << " teaspoons salt" << endl;
    cout << cupsFlour << " cups of flour" << endl;
    cout << cupsChocolateChips << " cups of chocolate chips" << endl;

    return 0;
}
