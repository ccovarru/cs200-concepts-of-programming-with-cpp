#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "Menu.hpp"

int RollDie();
void Program1();
void Program2();

int main()
{
    srand( time( NULL ) );

    bool done = false;
    while ( !done )
    {
        Menu::Header( "Dynamic Arrays Lab" );
        int choice = Menu::ShowIntMenuWithPrompt(
        {
            "Program 1: New and Delete",
            "Program 2: Resize Array",
            "Quit"
        } );

        switch( choice )
        {
            case 1: Program1(); break;
            case 2: Program2(); break;
            default: done = true;
        }
    }

    return 0;
}

void Program1()
{
    Menu::Header( "Program 1: New and Delete" );

    int totalDieRolls;
    int * dieRolls;
    float rollSum = 0;
    float averageValue;

    cout << "How many die rolls? ";
    cin >> totalDieRolls;

    dieRolls = new int[ totalDieRolls ];

    cout << "Rolling die... ";
    for ( int i = 0; i < totalDieRolls; i++ )
    {
        dieRolls[i] = RollDie();
        cout << dieRolls[i] << " ";
        rollSum += dieRolls[i];
    }

    averageValue = rollSum / totalDieRolls;
    cout << endl << "The average value is: " << averageValue << endl;

    delete [] dieRolls;

    cout << endl << endl;
}

void Program2()
{
    Menu::Header( "Program 2: Resize Array" );

    int size1, size2;

    cout << "Enter the size of the first array: ";
    cin >> size1;

    cout << "Enter the size of the second array (bigger than the first): ";
    cin >> size2;

    // Create array and fill it
    int * arr1 = new int[size1];
    cout << "Array contents: ";
    for ( int i = 0; i < size1; i++ )
    {
        arr1[i] = RollDie();
        cout << arr1[i] << " ";
    }
    cout << endl;

    // Resizing:
    // 1. Allocate space for the new array
    int * arr2 = new int[size2];

    // 2. Copy values from old array to new array
    for ( int i = 0; i < size1; i++ )
    {
        arr2[i] = arr1[i];
    }

    // 3. Free old memory
    delete [] arr1;

    // 4. Update pointer for arr1
    arr1 = arr2;

    // Fill the rest of the array up:
    for ( int i = size1; i < size2; i++ )
    {
        arr1[i] = RollDie();
    }

    // Display entire array
    cout << "Array contents: ";
    for ( int i = 0; i < size2; i++ )
    {
        cout << arr1[i] << " ";
    }
    cout << endl << endl;
}

int RollDie()
{
    return rand() % 6 + 1;
}
