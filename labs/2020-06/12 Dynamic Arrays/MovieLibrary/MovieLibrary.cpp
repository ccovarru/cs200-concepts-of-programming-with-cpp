#include "MovieLibrary.hpp"

#include <iostream>
#include <fstream>
using namespace std;

MovieLibrary::MovieLibrary()
{
    m_arraySize = 10;
    m_movieArray = new string[m_arraySize];
    m_itemsStored = 0;
    Load();
}

MovieLibrary::~MovieLibrary()
{
    Save();
    if ( m_movieArray != nullptr )
    {
        delete [] m_movieArray;
    }
}


void MovieLibrary::ViewAllMovies() const
{
    for ( int i = 0; i < m_itemsStored; i++ )
    {
        cout << i << ". " << m_movieArray[i] << endl;
    }
}

void MovieLibrary::ClearAllMovies()
{
    delete [] m_movieArray;
    m_movieArray = nullptr;
    m_arraySize = 0;
    m_itemsStored = 0;
}

void MovieLibrary::UpdateMovie( int index, string newTitle )
{
    m_movieArray[index] = newTitle;
}

void MovieLibrary::AddMovie( string newTitle )
{
    if ( m_movieArray == nullptr )
    {
        m_arraySize = 10;
        m_movieArray = new string[m_arraySize];
    }
    if ( IsFull() )
    {
        Resize();
    }

    m_movieArray[ m_itemsStored ] = newTitle;
    m_itemsStored++;
}

int MovieLibrary::GetMovieCount()
{
    return m_itemsStored;
}

bool MovieLibrary::IsFull()
{
    return m_itemsStored == m_arraySize;
}

void MovieLibrary::Resize()
{
    // Allocate more memory
    int newSize = m_arraySize + 10;
    string * newArray = new string[newSize];

    // Copy data to new array
    for ( int i = 0; i < m_arraySize; i++ )
    {
        newArray[i] = m_movieArray[i];
    }

    // Free memory of old array
    delete [] m_movieArray;

    // Update the pointer to point at new array
    m_movieArray = newArray;

    // Update the array size
    m_arraySize = newSize;
}

void MovieLibrary::Save()
{
    ofstream output( "movies.txt" );
    for ( int i = 0; i < m_itemsStored; i++ )
    {
        output << m_movieArray[i] << endl;
    }
    output.close();
}

void MovieLibrary::Load()
{
    ifstream input( "movies.txt" );
    string movieTitle;

    while ( getline( input, movieTitle ) )
    {
        AddMovie( movieTitle );
    }

    input.close();
}
