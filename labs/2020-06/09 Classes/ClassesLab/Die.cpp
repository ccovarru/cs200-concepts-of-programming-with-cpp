#include "Die.hpp"

Die::Die()
{
    sides = 6;
}

Die::Die( int sideCount )
{
    sides = sideCount;
}

int Die::Roll()
{
    if ( sides == 10 )
    {
        return rand() % sides;
    }

    return rand() % sides + 1;
}
