\contentsline {chapter}{\numberline {1}Setup}{2}
\contentsline {section}{\numberline {1.1}Turn-in instructions}{2}
\contentsline {section}{\numberline {1.2}Setup}{2}
\contentsline {chapter}{\numberline {2}Function lab 1: Percent to Decimal}{3}
\contentsline {paragraph}{Program overview:}{3}
\contentsline {section}{\numberline {2.1}Specifications}{3}
\contentsline {paragraph}{Program flow:}{4}
\contentsline {section}{\numberline {2.2}Testing:}{4}
\contentsline {chapter}{\numberline {3}Function lab 2: Price and Tax}{5}
\contentsline {paragraph}{Program overview:}{5}
\contentsline {section}{\numberline {3.1}Reference information}{5}
\contentsline {subsection}{\numberline {3.1.1}How do I only put 2 numbers after the decimal?}{5}
\contentsline {section}{\numberline {3.2}Specifications}{6}
\contentsline {section}{\numberline {3.3}Testing:}{7}
\contentsline {chapter}{\numberline {4}Function lab 3: Count Change}{8}
\contentsline {paragraph}{Program overview:}{8}
\contentsline {section}{\numberline {4.1}Specifications}{8}
\contentsline {section}{\numberline {4.2}Testing:}{9}
\contentsline {chapter}{\numberline {5}Function lab 4: Get distance}{11}
\contentsline {paragraph}{Program overview:}{11}
\contentsline {section}{\numberline {5.1}Reference information}{11}
\contentsline {subsection}{\numberline {5.1.1}The cmath library}{11}
\contentsline {section}{\numberline {5.2}Specifications}{12}
\contentsline {section}{\numberline {5.3}Testing:}{13}
