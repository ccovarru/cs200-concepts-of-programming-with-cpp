\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Variables lab}
\newcommand{\laTitle}       {CS 200}

\renewcommand{\chaptername}{Part}
\renewcommand{\contentsname}{Contents - \laTopic}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ / \laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

	\tableofcontents

\chapter{Turn-in instructions}

	\begin{itemize}
		\item	Once done, upload the .cpp files for each of the two lab parts; ~\\
				e.g.: \texttt{recipe.cpp} and \texttt{student.cpp}.
		\item	Don't zip the source files.
		\item	Don't zip the entire folder and upload that. I only want source files.
	\end{itemize}


\chapter{Reference - Project setup}

\paragraph{Visual Studio:}

\begin{enumerate}
	\item	Click on \textbf{Create a new project}.
	\item	Select \textbf{Empty Project} and click Next.
	\item	Set the project name and location, and click Create.
	\item 	Right-click on the project, go to Add and select \textbf{New item...}
	\item	Select C++ File (.cpp), give it a name, and click Add.
\end{enumerate}

\paragraph{Code::Blocks:}

\begin{enumerate}
	\item	Go to File $\to$ New $\to$ Project...
	\item	Select \textbf{Empty project} and click Go.
	\item	Set the project title and folder, and click Next.
	\item	Compiler options / leave default, click Finish.
	\item	Go to File $\to$ New $\to$ Empty file.
	\item	Save it as a .cpp file.
\end{enumerate}

\chapter{Variables lab 1: Recipe program}

For this lab, we're going to write a simple recipe program that will
display a cookie recipe to the user while also allowing them to
change the batch size (e.g., half a batch, double batch, etc).

~\\
You can use this as the starter code:

\begin{lstlisting}[style=code]
#include <iostream>
using namespace std;

int main()
{
    // 1. Declare ingredients variables

    // 2. Display ingredients to the screen

    // 3. Create a variable for "batchSize"

    // 4. Ask the user how many batches to make

    // 5. Recalculate ingredient amounts

    // 6. Display updated ingredient list

    return 0;
}
\end{lstlisting}

\section{Step 1: Declare ingredient variables}
Declare float variables for each of the following ingredients,
and initialize them to the values given:

\begin{center}
\begin{tabular}{l l c}
	Ingredient 		& Suggested variable name 		& Initial value \\ \hline
	Butter 			& \texttt{ cupsOfButer }		& 1	\\
	Sugar 			& \texttt{ cupsOfSugar }		& 1	\\
	Eggs 			& \texttt{ eggs }				& 2	\\
	Vanilla			& \texttt{ tspsVanilla }		& 2	\\
	Baking Soda		& \texttt{ tspsBakingSoda }		& 1	\\
	Salt			& \texttt{ tspsSalt }			& 0.5	\\
	Flour			& \texttt{ cupsFlour }			& 3	\\
	Chocolate Chips	& \texttt{ cupsChocolateChips }	& 2	\\
\end{tabular}
\end{center}

\begin{hint}{Hint: How do I declare and initialize a float variable?}
~\\
A \textbf{variable declaration} must have, at least, 
the \textbf{data type}
and the \textbf{variable name (aka identifier)}.

\begin{lstlisting}[style=code]
float price;
\end{lstlisting}

You can \textbf{initialize} the variable while \textbf{declaring} it
during the same line of code by using the \textbf{assignment operator}
and putting its starter value on the right-hand side of the equal sign.

\begin{lstlisting}[style=code]
float price = 9.99;
\end{lstlisting}

\end{hint}

\section{Step 2: Display ingredients to the screen}

Next, use the \texttt{cout} command to display each variable's value,
along with a label of what the variable represents.

\paragraph{Concept: User interface design -}
If you only output the variable, then when the program is run it will just display the number itself:

\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
        
Code
        
\begin{lstlisting}[style=code]
cout << cupsOfButter;
\end{lstlisting}
           
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
    
Program output
    
\begin{lstlisting}[style=output]
1
\end{lstlisting}

    \end{subfigure}
\end{figure}

Which is confusing - to a user trying out your program, what does ``1'' mean?
Instead, you should also display \textbf{string literals} to give context
to what is going on in the program...

\begin{lstlisting}[style=code]
cout << "Sugar: " << cupsOfSugar << " cup(s)" << endl;
\end{lstlisting}

And this would give more information to the user:

\begin{lstlisting}[style=output]
Sugar: 1 cup(s)
\end{lstlisting}

You can format the text however you'd like, but you should display
some sort of label along with each variable.

\begin{center}
	\includegraphics[width=5cm]{images/lab01-ingredients.png}
\end{center}

\section{Step 3: Create a variable for ``batchSize''}

This will just be a simple float variable declaration. You do not need
to initialize it, since the user will be entering a value for it.

\section{Step 4: Ask the user how many batches to make}

For this step, you will use \texttt{cin} to get information from the
keyboard and store it in the \texttt{batchSize} variable.

\paragraph{Concept: User interface design -}

If you write a \texttt{cin} statement without adding a \texttt{cout}
statement right before it, the program will sit there and wait for
the user to enter information, but the user won't be able to tell
that it's waiting on them.

\textbf{Any time you ask the user for input with \texttt{cin},
you should always have some text output with \texttt{cout} telling
them what kind of information you want from them.}

~\\
Bad design:

\begin{lstlisting}[style=code]
cin >> batchSize;
\end{lstlisting}

~\\
Good design:

\begin{lstlisting}[style=code]
cout << "How many batches? ";
cin >> batchSize;
\end{lstlisting}



\section{Step 5: Recalculate ingredient amounts}
Next, we will be updating each of the ingredient amounts, multiplying
the original ingredient sizes by the batch size.

~\\
You can take the existing value of \texttt{cupsOfButter}, multiply it
by the \texttt{batchSize}, and store it back in the \texttt{cupsOfButter}
variable like this:

\begin{lstlisting}[style=code]
cupsOfButter = cupsOfButter * batchSize;
\end{lstlisting}


\section{Step 6: Display updated ingredient list}

Finally, just like in step 2, display all the ingredients again.
Since we did the math in Step 5, the ingredient amounts will be
adjusted based on what the user wanted.

\section{Example output}

~\\ \textbf{Making a half-batch:}

\begin{lstlisting}[style=output]
How many batches? 0.5

ADJUSTED INGREDIENTS LIST
0.5 cups of butter
0.5 cups of sugar
1 eggs
1 teaspoons vanilla
0.5 teaspoons baking soda
0.25 teaspoons salt
1.5 cups of flour
1 cups of chocolate chips
\end{lstlisting}


~\\ \textbf{Making a double-batch:}

\begin{lstlisting}[style=output]
How many batches? 2

ADJUSTED INGREDIENTS LIST
2 cups of butter
2 cups of sugar
4 eggs
4 teaspoons vanilla
2 teaspoons baking soda
1 teaspoons salt
6 cups of flour
4 cups of chocolate chips

\end{lstlisting}



\newpage
\chapter{Variables lab 2: Student information}

Create a new project/solution for this new project.
You will also need to create a new source file, such as \texttt{student.cpp}.

You can start out with this code:

\begin{lstlisting}[style=code]
#include <iostream>
#include <string>
using namespace std;

int main()
{
    // 1. Declare and initialize variables

    // 2. Display student information

    // 3. Have user update info

    // 4. Display updated info

    return 0;
}
\end{lstlisting}

Note that we will have another \#include here to add the \textbf{string} data type.
Sometimes if you forget this \#include statement, Visual Studio will give you
weird compile errors that aren't very descriptive. So if something's acting weird,
make sure you have that include above!

\section{1. Declare and initialize variables}
Declare and initialize five variables according to the following table:

\begin{center}
\begin{tabular}{l l c}
	Variable name 					& Data type	& Initial value \\ \hline
	\texttt{ email }				& string	& \texttt{"student@jccc.edu"}
	\\
	\texttt{ grade }				& char		& \texttt{'A'}
	\\
	\texttt{ gpa }					& float		& \texttt{3.50}
	\\
	\texttt{ semestersCompleted }	& int		& \texttt{2}
	\\
	\texttt{ degreeSeeking }		& bool		& \texttt{true}
\end{tabular}
\end{center}

Note that string values must be written in double-quotes and that
char values must be within single-quotes.

\section{2. Display student information}

Display all of the variables listed above, along with labels for each,
so that the program output is clear.

\begin{lstlisting}[style=output]
STUDENT INFORMATION
Email:               	student@jccc.edu
Grade:               	A
GPA:                 	3.98
Semesters completed: 	2
Degree seeking?:     	1
\end{lstlisting}

\begin{hint}{Text alignment?}
There are ways to align text in a table form in C++, but we will cover that later on. 
For now, I've just aligned my variable outputs by adding spaces to the string literals:
\begin{lstlisting}[style=code]
cout << "STUDENT INFORMATION" << endl;
cout << "Email:                 " << email << endl;
cout << "Grade:                 " << grade << endl;
\end{lstlisting}
\end{hint}

\section{3. Have user update info}

Next, you will have a series of \texttt{cout} and \texttt{cin} statements
to have the user enter new values for each variable.

~\\ Example output:
\begin{lstlisting}[style=output]
UPDATE STUDENT INFO
Enter email: rsingh13@jccc.edu
Enter grade (A, B, C, D, or F): F
Enter gpa: 0.50
Enter semesters completed: 10
Enter degree seeking (1 = yes, 0 = no): 0
\end{lstlisting}

With the boolean variable, the user needs to enter 1 for \texttt{true}, or 0 for \texttt{false}.
It won't work if they type in the text ``true'' and ``false''.


\section{4. Display updated info}

Just like in step 2, display the updated user information.

\section{Example output}

\begin{lstlisting}[style=output]
STUDENT INFORMATION
Email:                 student@jccc.edu
Grade:                 A
GPA:                   3.98
Semesters completed:   2
Degree seeking?:       1

UPDATE STUDENT INFO
Enter email: rsingh13@jccc.edu
Enter grade (A, B, C, D, or F): F
Enter gpa: 0.50
Enter semesters completed: 10
Enter degree seeking (1 = yes, 0 = no): 0

UPDATED STUDENT INFORMATION
Email:                 rsingh13@jccc.edu
Grade:                 F
GPA:                   0.50
Semesters completed:   10
Degree seeking?:       0
\end{lstlisting}



\newpage
\chapter{Compile error help}

\textbf{undeclared identifier} (Visual Studio) ~\\
\textbf{'thing' was not declared in this scope} (Code::Blocks) ~\\
It is looking for a variable by this name, but cannot find it. Perhaps you have a typo or have misspelled a command.

~\\
\textbf{expected '\}' at the end of input} ~\\
You're missing a closing curly-brace somewhere, probably at the end of the program.



\end{document}

