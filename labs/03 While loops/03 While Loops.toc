\contentsline {chapter}{\numberline {1}Setup}{3}
\contentsline {section}{\numberline {1.1}Turn-in instructions}{3}
\contentsline {section}{\numberline {1.2}Setup}{3}
\contentsline {subsection}{\numberline {1.2.1}Using the starter code}{3}
\contentsline {paragraph}{Project setup:}{3}
\contentsline {subparagraph}{Add existing file in Visual Studio:}{3}
\contentsline {subparagraph}{Add existing file in Code::Blocks:}{4}
\contentsline {subsection}{\numberline {1.2.2}Writing starter code from scratch}{4}
\contentsline {paragraph}{Project setup:}{4}
\contentsline {subsection}{\numberline {1.2.3}About the starter code}{6}
\contentsline {chapter}{\numberline {2}While loops lab 1: Count Up}{7}
\contentsline {paragraph}{Program overview:}{7}
\contentsline {section}{\numberline {2.1}Reference information}{7}
\contentsline {subsection}{\numberline {2.1.1}How do I use a while loop?}{7}
\contentsline {section}{\numberline {2.2}Specifications}{8}
\contentsline {paragraph}{Program flow:}{8}
\contentsline {section}{\numberline {2.3}Testing:}{8}
\contentsline {chapter}{\numberline {3}While loops lab 2: Multiply up}{9}
\contentsline {paragraph}{Program overview:}{9}
\contentsline {section}{\numberline {3.1}Reference information}{9}
\contentsline {subsection}{\numberline {3.1.1}Math operations}{9}
\contentsline {section}{\numberline {3.2}Specifications}{10}
\contentsline {paragraph}{Program flow:}{10}
\contentsline {section}{\numberline {3.3}Testing:}{10}
\contentsline {chapter}{\numberline {4}While loops lab 3: Number guesser}{11}
\contentsline {paragraph}{Program overview:}{11}
\contentsline {section}{\numberline {4.1}Reference information}{11}
\contentsline {subsection}{\numberline {4.1.1}Do... while loops}{11}
\contentsline {section}{\numberline {4.2}Specifications}{12}
\contentsline {paragraph}{Program flow:}{12}
\contentsline {section}{\numberline {4.3}Testing:}{12}
\contentsline {chapter}{\numberline {5}While loops lab 4: Input validator}{14}
\contentsline {paragraph}{Program overview:}{14}
\contentsline {section}{\numberline {5.1}Reference information}{15}
\contentsline {subsection}{\numberline {5.1.1}Is the number within a valid range?}{15}
\contentsline {paragraph}{Valid entry:}{15}
\contentsline {paragraph}{Invalid entry:}{15}
\contentsline {section}{\numberline {5.2}Specifications}{15}
\contentsline {paragraph}{Program flow:}{15}
\contentsline {section}{\numberline {5.3}Testing:}{16}
\contentsline {paragraph}{Program overview:}{17}
\contentsline {section}{\numberline {5.4}Specifications}{17}
\contentsline {paragraph}{Variables:}{17}
\contentsline {paragraph}{Program information:}{17}
\contentsline {section}{\numberline {5.5}Testing:}{18}
\contentsline {chapter}{\numberline {6}While loop lab 6: Summation}{19}
\contentsline {paragraph}{Program overview:}{19}
\contentsline {section}{\numberline {6.1}Specifications}{19}
\contentsline {paragraph}{Variables:}{19}
\contentsline {paragraph}{Program information:}{20}
\contentsline {section}{\numberline {6.2}Testing:}{20}
