\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Pseudocode, Flowcharts, and Planning}
\newcommand{\laTitle}       {}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ }

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

\chapter*{Tech Literacy Lab: Pseudocode, Flowcharts, and Planning.}

\section*{Lab instructions}

	At the end of this document is a set of questions to answer. You can
	fill it out on the computer or on paper. Turn in a \textbf{digital copy}
	of your work to Canvas. You can use an app like \textbf{Genius Scan}
	to photograph and digitize your work if you don't have a scanner.

\hrulefill
\section*{Introduction}

	\paragraph{What is a program?} If you boil down all the techniques
	and structures that are part of a programming language, when it comes
	down to it a program is a set of instructions that execute one-after-another,
	and you can add \textbf{branching} and \textbf{loops} to affect
	the order the instructions run in. We get \textbf{user input} and that
	affects how the program \textbf{flows} (using branches and loops),
	and we display \textbf{output} to let the user know what the result was.

\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
      
           We make decisions all the time, but perhaps we don't think
           about it explicitly. Let's say we are a waiter at a
           restaurant that happens to serve beer. A customer might
           be any age and order a salad or a burger or anything else -
           in these cases, we don't care about age. However, \textit{if}
           the customer asks for a beer, their age must be confirmed and
           a decision made based on that: Give beer? Don't give beer?
           ~\\~\\
           Here we'd have an \textbf{if/else} statement: If customer
           wants beer... else (they want anything else)...
           
			And if a second \textbf{if/else} statement if they want beer:
			If customer age is 21 or over... else (they are under 21)...
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
    
        \centering
           \includegraphics[width=8cm]{images/beer-chart.png}
           
    \end{subfigure}
\end{figure}

	With computer software, often we are \textbf{abstracting}
	something from the real world; how a waiter takes an order,
	or how a patron can check out a library book, or how a vending
	machine takes money and a number code and returns a snack.
	
	And, when we are designing how a program \textit{works}, it is important
	to start with \textbf{planning}. Many new programming students will
	begin to tackle a program by \textit{immediately opening their IDE}
	(Integrated Develpoment Environment) and begin typing out code.
	However, it can be hard to conceptualize how the program will flow
	early on, and you can get confused very quickly.
	Once you have more experience, you will become better at 
	thinking of \textit{how an algorithm or program would flow} before
	getting into the coding of it, but early on you should practice 
	planning out your program ahead of time.
	
	\begin{center}
		\includegraphics[width=6cm]{images/what-am-i-doing.png}
	\end{center}

	\begin{center}
		\large
		\textit{``Weeks of coding can save hours of planning''}
		~\\
		\raggedleft
		\normalsize
		- Unknown
	\end{center}
	
	\newpage
	
	\newpage
	\section*{Flowcharts}
	
	We can use flowcharts to plan the \textbf{flow} of a program on
	paper before we start implementing any code. There are different
	shapes that indicate different \textit{types} of programming
	commands, with arrows going between points to show the overall flow
	of the software.
	
\begin{figure}[h]
    \centering
    \begin{subfigure}{.6\textwidth}
      
	These commands are the core building blocks of programming.
		
    \end{subfigure}%
    \begin{subfigure}{.4\textwidth}
    
        \centering
           \includegraphics[width=4cm]{images/building-blocks.png}
           
    \end{subfigure}
\end{figure}
	
	\begin{center}
		\begin{tabular}{ p{4cm} p{4cm} p{4cm} }
			\begin{tikzpicture}
				\draw (0,0) ellipse (1cm and 0.5cm);
			\end{tikzpicture}
			&
			\begin{tikzpicture}
				\draw (0,0) -- (2,0) -- (2,1) -- (0,1) -- (0,0);
			\end{tikzpicture}
			&
			\begin{tikzpicture}
				\draw (0,0) -- (2,0) -- (2.5,1) -- (0.5,1) -- (0,0);
			\end{tikzpicture}
			\\
			\footnotesize
			\textbf{Oval:} 
			
			Start/end of a program
			&
			\footnotesize
			\textbf{Rectangle:} 
			
			Something to process
			&
			\footnotesize
			\textbf{Parallelogram:} 
			
			Get input or display output
			
			\\ \\
			
			\begin{tikzpicture}
				\draw (-1,0) -- (0,-0.5) -- (1, 0) -- (0, 0.5) -- (-1, 0);
			\end{tikzpicture}
			&
			\begin{tikzpicture}
				\draw[-stealth,ultra thick]      (0,0) -- (2,0);
			\end{tikzpicture}
			\\
			\footnotesize
			\textbf{Diamond:}
			
			Make a decision
			&
			\footnotesize
			\textbf{Arrow:}
			
			Indicates flow between steps
			
		\end{tabular}
	\end{center}
	
	\paragraph{Example 1: Room area calculator (Linear program)} ~\\
	
\begin{figure}[h]
    \centering
    \begin{subfigure}{.6\textwidth}
      
	Let's say we want to make a simple program that will calculate
	the area of a room. The formula for the area of a rectangle
	is ~\\ \textbf{Area = Width x Length}. ~\\
	
	For a user to find this program helpful, they will need to be
	able to enter their own numbers for \textbf{width} and
	\textbf{length}, and then the program can calculate the area
	and display the result.
		
    \end{subfigure}%
    \begin{subfigure}{.4\textwidth}
    
        \centering
           \includegraphics[width=4cm]{images/area.png}
           
    \end{subfigure}
\end{figure}

	Diagramming this program, we can think about what the user sees
	to help us figure out how the program will flow...
	
	\begin{hint}{Thinking about what the user will see}
		\begin{enumerate}
			\item	``AREA FINDER PROGRAM''
			\item	``Please enter the room length:''
			\item	User enters the length
			\item	``Please enter the room width:''
			\item	User enters the width
			\item	``The area is:''
			\item	Program displays the Area
			\item	The end!
		\end{enumerate}
	\end{hint}
	
	And, we can build out the flowchart:
	
	\begin{center}
		\includegraphics[width=5cm]{images/area-flowchart.png}
	\end{center}
	
	\newpage
	And from the flowchart, we can build out some code:
	
	~\\ \textbf{Python code:}
\begin{lstlisting}[style=pycode]
print( "AREA FINDER" )

L = input( "What is the length? " )
W = input( "What is the width? " )

A = L * W

print( "The area is:" )
print( A )
\end{lstlisting}

	~\\ \textbf{C++ code (partial):}
\begin{lstlisting}[style=code]
cout << "AREA FINDER" << endl;

cout << "What is the length? ";
cin >> L;

cout << "What is the width? ";
cin >> W:

A = L * W

cout << "The area is:" << A << endl;
\end{lstlisting}
	~\\~\\
	
	For this exercise we aren't going to be converting these
	flowcharts to code. I just wanted to show each step of the planning
	process to help you in the future.
	
	\newpage
	\paragraph{Example 2: Do you like cats? (With branching)}
	
	This program will use the decision diamond to give a different response
	to the user based on what their input was. In the previous example,
	we just took some numbers and did math - there was no branching
	and no loops. Here, we have slightly different behavior based on
	what the user gives us.
	
	\begin{center}
		\includegraphics[width=8cm]{images/flowchart-cats.png}
	\end{center}
	
	If the user says ``no'' in response to being asked if they like
	cats, then the program will display a sad face. If they say ``yes'',
	then it will display a happy face.
	
	\newpage
	\paragraph{Example 3: Countdown (with loops)}
	
	This program doesn't take any user input (though we could modify
	it to do so). It just has a counter variable and counts down,
	only ending the program once the \texttt{count} hits 0.
	
	\begin{center}
		\includegraphics[width=8cm]{images/flowchart-countdow.png}
	\end{center}
	
	In our programming languages, we have special \textbf{loop} structures
	we can use, but for flowcharts we can build a loop with the
	\textbf{decision diamond} and having our arrows point back to that
	decision.
	
	% ---------------------------------------------------------------- %
	\newpage
	\section*{Pseudocode}
	
	Another tool you'll come across is \textbf{pseudocode}. Basically,
	writing out the steps of a program in text - some sort of human
	readable form - but it not being actual program code. You will
	usually see pseudocode when there is an established \textbf{algorithm}
	and pseudocode is provided so that you can ``translate'' it into
	any other programming language.

\paragraph{Example 1: Check two objects' positions on an X,Y plane} ~\\
	
\begin{lstlisting}[style=output]
BEGIN with x1, y1, x2, y2:

	If x1 is less than x2, then...
		object1 is "left" of object2
	Else if x1 is greater than x2, then...
		object1 is "right" of object2
	Else...
		object1 and object2 are 
		at the same horizontal position

	If y1 is less than y2, then...
		object1 is "above" object2
	Else if y1 is greater than y2, then...
		object1 is "below" object2
	Else...
		object1 and object2 are at 
		the same vertical position
	
END
\end{lstlisting}

\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
        \centering
           \includegraphics[width=6.5cm]{images/coordinates.png}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        
        Note: in computer graphics, the origin (0, 0) is at the top-left
		of the screen. A larger $x$ is further right, a larger $y$ is further down.
    \end{subfigure}
\end{figure}

	\newpage
	If you don't have time to draw out an entire flowchart, it can still
	be useful to plan out your program as pseudocode first to iron out
	all the steps you'll need.
	
	Writing out pseudocode could be good as you're trying to design a solution,
	even if you don't know what the flow should be like yet. Start with one step
	at a time, and think through what is needed each step and you write it down.
	~\\~\\
	
	Sometimes, you'll have a higher-level idea of a step that needs to be done,
	and you can write that out.
	
	\begin{center}
	\textbf{``Calculate perimeter of room''}
	\end{center}
	
	Once you realize you need to take that step, you can make it more fine-grained,
	adding more detail to what needs to be done to accomplish that step...
	
	\begin{center}
	\textbf{``Calculate perimeter of room:''} ~\\
	Get width from user ~\\
	Get height from user ~\\
	Calculate 2 * width + 2 * height
	\end{center}
	
\newpage
\paragraph{Example 2: Calculating interest over time} ~\\

	Let's make some pseudocode that will calculate the amount of accumulated
	interest over time, given some base amount and some interest rate.

\begin{lstlisting}[style=output]
Ask user for their student loan balance.
Store input in BALANCE.

Ask user for their student loan interest.
Store input in INTEREST.

Ask user for amount of years.
Store input in TOTALYEARS.

Create variable YEAR, set to 1.

While YEAR is less than or equal to TOTALYEARS, loop...
	Take BALANCE x INTEREST and store as INCREASE.
	Add INCREASE to BALANCE and store as BALANCE.
	Display YEAR and BALANCE.
	YEAR = YEAR + 1	
\end{lstlisting} ~\\

	For this pseudocode, the user enters three things: Their loan
	balance, the interest rate, and the totals years to calculate
	the amount interest will cost them. If we were to step through the
	code ourselves, pretending to be the computer, it would work like this...
	
	\newpage
	\begin{itemize}
		\item	BALANCE = \$30,000		(Example value)
		\item	INTEREST = 0.0655	(Example value)
		\item	TOTALYEARS = 3
		
		\item	YEAR = 1...
		\begin{itemize}
			\item	INCREASE = BALANCE (\$30,000) x INTEREST (0.0655) \\ = \$1,965
			\item	BALANCE = BALANCE (\$30,000) + INCREASE (\$1,965) \\ = \$31,965
			\item	YEAR = 2
		\end{itemize}
		
		\item	YEAR = 2...
		\begin{itemize}
			\item	INCREASE = BALANCE (\$31,965) x INTEREST (0.0655) \\ = \$2,093.71
			\item	BALANCE = BALANCE (\$31,965) + INCREASE (\$2,093.71) \\ = \$34,058.71
			\item	YEAR = 3
		\end{itemize}
		
		\item	YEAR = 3...
		\begin{itemize}
			\item	INCREASE = BALANCE (\$34,058.71) x INTEREST (0.0655) \\ = \$2,230.85
			\item	BALANCE = BALANCE (\$34,058.71) + INCREASE (\$2,230.85) \\ = \$36,289.56
			\item	YEAR = 4
		\end{itemize}
	\end{itemize}
	
	And, once YEAR = 4, our loop would stop.
	~\\~\\
	
	
	% ---------------------------------------------------------------- %
	\newpage
	\section*{Testing}
	
	\begin{center}
	\includegraphics[width=8cm]{images/validation.png}
	\end{center}
	
	Another skill that is good to develop as a software developer is how
	to test and validate your work. You can break down parts of your code
	into little transactions of ``inputs'' and ``outputs'', and then
	develop \textbf{test cases}.
	
	\paragraph{Test case:} A test case is a single test.
	You specify some \textbf{input(s)} that you will give your program,
	and the \textbf{expected output(s)} it should return.
	
	~\\
	When you run the actual program with your inputs, it will return
	\textbf{actual output(s)}. Compare the actual output with the
	expected output to validate whether the program worked as expected.
	
	~\\
	Test cases can be built without any of the program built so far.
	In fact, it can be handy to write your tests \textit{ahead of time}
	so you have a better understanding of how things are supposed to work.
	
	\newpage
	\paragraph{Example 1: Bank withdrawals} ~\\
	In this example, the program keeps track of a bank balance and the
	amount the user wants to withdraw, but it shouldn't let the balance fall
	below 0. 
	
	~\\
	\begin{tabular}{c p{3cm} p{4cm} p{3cm}}
		\textbf{Test case} & \textbf{Inputs} & \textbf{Expected output} & \textbf{Actual output}
		\\ \hline
		1 
		& balance = 100
		
		withdraw = 10
		&
		balance is now 90
		\\ \hline
		2
		& balance = 0
		
		withdraw = 100
		&
		can't withdraw!
		\\ \hline
		3
		& balance = 100
		
		withdraw = 100
		&
		balance is now 0
	\end{tabular}
	
	~\\~\\
	You would make a list of inputs and what \textit{should} be the result
	for each case, and then run your program and check each scenario.
	If something doesn't match, it could mean that there's an error in a
	calculation somewhere, or other logic in the program.
	(And yes, sometimes tests can be wrong, too.)
	
	\paragraph{Example 2: Calculate price plus tax} ~\\
	When ringing up a product at a store, there will be a price for that product
	and a sales tax that gets added onto it. Using a test case can
	help us make sure that the program's calculations are correct.
	
	~\\
	Let's say that the programmer implemented the formula like this:
	
	\begin{center}
	\texttt{total = price + tax}
	\end{center}
	
	We could validate the program's actual output vs. the expected output,
	and see that there's an error.	
	
	~\\
	\begin{tabular}{c p{3cm} p{4cm} p{3cm}}
		\textbf{Test case} & \textbf{Inputs} & \textbf{Expected output} & \textbf{Actual output}
		\\ \hline
		1 
		& price = \$10.00
		
		tax = 0.09
		&
		\color{black} total = \$10.90
		&
		\color{red}total = \$10.09 \color{black}
		
		\\ \hline
		2 
		& price = \$5.45
		
		tax = 0.06
		&
		\color{black} total = \$5.78
		&
		\color{red}total = \$5.51 \color{black}
		\\ \hline
		3 
		& price = \$2.25
		
		tax = 0.095
		&
		\color{black} total = \$2.46
		&
		\color{red}total = \$2.35 \color{black}
	\end{tabular}
	
	~\\
	What is the problem with the formula? Well, we don't just \textit{add tax}
	as a flat number; ``0.09'' is supposed to represent 9\%, not 9 cents!
	The correct formula would be \tab
	\texttt{total = price + (price * tax)}
	
	
	
	
	% ---------------------------------------------------------------- %
	\newpage
	\section*{Programming Tips}
	
	\begin{center}
	\includegraphics[width=8cm]{images/a-billion-errors.png}
	\end{center}

	\begin{itemize}
		\item	When just starting out, only write a few lines of code at a time.
				After that, build and make sure you have no syntax errors before continuing.
		\item	Once you're a little better, only implement \textbf{one feature at a time}.
				Build and run and test before moving on to the next feature.
		\item	\textbf{Don't} try to write the entire program and then build and run at the very end.
				If you have syntax errors, it will be harder to track them down.
		\item	Write out your plan (flowchart, pseudocode) while reading through the program
				specifications. Often, the specifications has a lot of information you'll need,
				but not in the order you'll write it or split into nice little steps.
		\item	If you're not sure where something is getting the wrong value, or at what point
				a program is crashing, add output statements throughout the program to see
				what your program is doing as it is running.
		\item	You can comment out lines of code here and there to try to locate errors.
		\item	Sometimes it helps to write a separate smaller program to test one little
				thing about your bigger program.
		\item	Make sure you learn the vocabulary used in programming; it will make
				communicating with other programmers, your classmates, and your instructors more clear,
				instead of saying something like ``My if thingy called the other thing with a, b, and c things
				and it didn't return the right value!!''
	\end{itemize}
	
	% ---------------------------------------------------------------- %
	% ---------------------------------------------------------------- %
\chapter*{Questions}

    \stepcounter{question} \question{\thequestion}{Flowchart Output}{10\%}
    Trace through the flowchart and write down all the program output based on the input given,
    and the values of every variable each time one changes.
    
	\begin{intro}{User input:}
		YES
		\tab \footnotesize
		(The only input is the \texttt{isSenior} variable step)
	\end{intro}
	
    \includegraphics[width=8cm]{images/flowchart-senior.png}

    \newpage
    Space to fill out question \thequestion.
    Pretend you're the computer processing
    each command, one at a time. List out what you're doing at each step
    and how any variables change and what is being inputted and outputted.
    ~\\
    \includegraphics[width=6cm]{images/starter1.png}
    
    \newpage
    \stepcounter{question} \question{\thequestion}{Flowchart Output}{10\%}
    Trace through the flowchart and write down all the program output based on the input given,
    and the values of every variable each time one changes.

	\begin{intro}{User input:}
		4
		\tab 2.00
		\tab 2.50
		\tab 5.45
		\tab 3.25
	\end{intro}
    
    \includegraphics[width=10cm]{images/flowchart-price.png}
    
    \newpage
    Space to fill out question \thequestion.
    ~\\
    \includegraphics[width=8cm]{images/starter.png}
    
	\newpage
    \stepcounter{question} \question{\thequestion}{Drawing Flowcharts}{10\%}
	Draw a flowchart for a program that does the following:
	
	\begin{itemize}
		\item	Displays text, ``please enter your name''.		(Parallelogram, output step)
		\item	Gets user input, stores as ``name''.			(Parallelogram, input step)
		\item	Displays the ``name'' variable to the user.		(Parallelogram, output step)
	\end{itemize}
	
	Make sure to specify when you're displaying output and getting input.
	All input has to be stored in some named variable.

	\newpage
    \stepcounter{question} \question{\thequestion}{Drawing Flowcharts}{10\%}
	Draw a flowchart for a program that does the following:
	
	\begin{itemize}
		\item	Program asks the user ``what is your age?'', gets their input, stores in a variable called ``age''.
		\item	If their age is 18 or older, display ``can vote.''
		\item	Otherwise, display ``cannot vote.''
	\end{itemize}
	
	\newpage
    \stepcounter{question} \question{\thequestion}{Drawing Flowcharts}{10\%}
	Draw a flowchart for a program that does the following:
	
	\begin{itemize}
		\item	Asks the user, ``what is your grade? (0 to 100)''
		\item	Stores the user input in a variable called ``grade''.
		\item	If grade is 90 or greater ($grade \geq 90$), display ``A''.
		\item	If grade is between 80 and 90 ($80 \leq grade < 90$), display ``B''.
		\item	If grade is between 70 and 80 ($70 \leq grade < 80$), display ``C''.
		\item	If grade is between 60 and 70 ($60 \leq grade < 70$), display ``D''.
		\item	If grade is below 60 ($grade < 60$), display ``F''.
	\end{itemize}
	
    \newpage
    Space to fill out question \thequestion.
    
	\newpage
    \stepcounter{question} \question{\thequestion}{Drawing Flowcharts}{10\%}
	Draw a flowchart for a program that does the following:
	
	\begin{itemize}
		\item	Create a variable called ``balance'', set it to 100.
		\item	While ``balance'' is greater than 0... (use decision diamond)
		\begin{itemize}	
			\item	Ask the user, ``how much to withdraw?''
			\item	Store user input in variable called ``withdraw''
			\item	Subtract ``withdraw'' from ``balance''
		\end{itemize}
		
		\item	Once ``balance'' is 0 or negative (exiting the decision diamond):
		\item	Display message ``out of money''.
	\end{itemize}

    \newpage
    Space to fill out question \thequestion.
    
	\newpage
    \stepcounter{question} \question{\thequestion}{Pseudocode}{10\%}
	Write out some basic pseudocode for the following:
	~\\~\\
	A program that asks the user for the price of 5 items (use different variable names for each)
	and calculates the sum of all those prices. Display the total price.
	
	\newpage
    \stepcounter{question} \question{\thequestion}{Pseudocode}{10\%}
	Write out some basic pseudocode for the following:
	~\\~\\
	A program that displays whether the person lives in ``MO'' or ``KS''
	based on the city they entered. Have at least four city options
	(e.g., ``Lee's Summit'', ``Olathe'', ``Raytown'', ``Overland Park'')
	to check for.
	
	\newpage
    \stepcounter{question} \question{\thequestion}{Test cases}{10\%}
	Fill out some example inputs and outputs for these test cases.
	The program calculates the average rating of a movie based on
	all user scores. Scores will be 1, 2, 3, 4, or 5 based on the
	amount of stars given.
	
	\begin{center}
		Average = (sum of scores) / (total number of scores)
	\end{center}

	\begin{center}
		\begin{tabular}{c p{5cm} p{5cm}}
			\textbf{Test case} & \textbf{Inputs} & \textbf{Expected output}
			\\ \hline
			1 & scores: 3, 3, 4, 5		& average: 3.75 stars
			\\ \\
			2 &
			\\ \\
			3 &
			\\ \\
			4 &
		\end{tabular}
	\end{center}
	
	\vspace{2cm}
	
    \stepcounter{question} \question{\thequestion}{Test cases}{10\%}
	Fill out some example inputs and outputs for these test cases.
	A movie theater offers tickets for \$10. If you're under 5, 
	tickets are \$5. If you're a senior, tickets are \$6. If you're
	a vet, tickets are \$6. If you're a senior and a vet, tickets are \$4.
	~\\~\\
	In this case, should be able to write test cases to cover all scenarios:
	Under 5, senior-and-vet, senior-and-not-a-vet, vet, and other.
	(We can assume any under-5-years-old customer can't be a vet.)

	\begin{center}
		\begin{tabular}{c p{5cm} p{5cm}}
			\textbf{Test case} & \textbf{Inputs} & \textbf{Expected output}
			\\ \hline
			1 & age: 4		
			
			vet: no
								& ticket price: \$5
			\\
			2 &
			\\ \\
			3 &
			\\ \\
			4 &
			\\ \\
			5 &
		\end{tabular}
	\end{center}
\end{document}





